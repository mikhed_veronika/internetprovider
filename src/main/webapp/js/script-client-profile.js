var collapseHeadersAccount = document.querySelectorAll(".collapse-header.account-accordion");
var collapseContentAccount = document.querySelectorAll(".collapse-content.account-accordion");
for (var i = 0; i < collapseHeadersAccount.length; i++) {
    collapseHeadersAccount[i].setAttribute("href", "#collapse"+ i);
    collapseContentAccount[i].setAttribute("id", "collapse"+i);
}


var collapseHeadersPayment = document.querySelectorAll(".collapse-header.payment-accordion");
var collapseContentPayment = document.querySelectorAll(".collapse-content.payment-accordion");
for (var i = 0; i < collapseHeadersPayment.length; i++) {
    collapseHeadersPayment[i].setAttribute("href", "#collapse-payment"+ i);
    collapseContentPayment[i].setAttribute("id", "collapse-payment"+i);
}