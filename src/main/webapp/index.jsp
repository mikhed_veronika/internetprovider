<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/fontello.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/site.css">
</head>
<body>

<div class="content">
    <nav class="navbar nav-color">
        <ul class="nav navbar-nav nav-top">
            <li><a href="index.jsp" class="text-main"><fmt:message key="starNet"/></a></li>
            <li><a href="/controller?command=tariffs" class="text-menu"><fmt:message key="tariffs"/></a></li>
            <li><a href="/controller?command=comments" class="text-menu"><fmt:message key="comments"/></a></li>
        </ul>
        <form class="lang-select">
            <select id="language" name="language" onchange="submit()" class="form-control">
                <option value="ru" ${language == 'ru' ? 'selected' : ''}><fmt:message key="ru"/> </option>
                <option value="en" ${language == 'en' ? 'selected' : ''}><fmt:message key="en"/> </option>
            </select>
        </form>
        <a href="/controller?command=redirectToLogin" class="btn btn-ref"><span class="icon-user"></span><fmt:message key="login"/></a>
    </nav>

    <button type="button" name="back" onclick="history.back()" class="request-btn btn button-back"><fmt:message key="back"/></button>

    <div class="content-main">
        <div class="jumbotron">
            <h2 class="header-main"><fmt:message key="starNetProvider"/></h2>
            <h1 class="header-main"><fmt:message key="welcomeLabel"/></h1>
            <p class="lead text-main"><fmt:message key="starNetMain"/></p>
        </div>

        <div class="row">
            <div class="col-md-4">
                <h2><fmt:message key="mainLogin"/></h2>
                <p>
                    <fmt:message key="mainLoginText"/>
                </p>
                <p><a class="btn btn-default" href="jsp/login.jsp"><fmt:message key="login"/> &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2><fmt:message key="mainTariff"/></h2>
                <p>
                    <fmt:message key="mainTariffText"/>
                </p>
                <p><a class="btn btn-default" href="/controller?command=tariffs"><fmt:message key="mainTariffBtn"/> &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2><fmt:message key="mainReview"/></h2>
                <p>
                    <fmt:message key="mainReviewText"/>
                </p>
                <p><a class="btn btn-default" href="/controller?command=comments"><fmt:message key="mainReviewBtn"/> &raquo;</a></p>
            </div>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf"%>

</body>
</html>

