<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="comments"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>

<body>
<div class="content">
    <%@include file="/WEB-INF/jspf/clientMenu.jspf"%>
    <div class="comments-container">
        <br/>
        <h3 class="header"><fmt:message key="comments"/></h3> <br/>
        <div class="add-btn-block">
            <a href="/controller?command=redirectToAddComment" class="add-comment"><span class="icon-book"></span><fmt:message key="addComment"/></a>
        </div>
        <br/>
        <c:forEach var="comment" items="${comments}">
            <div class="comment-block">
                <h4 class="comment-sender">${comment.firstName} ${comment.lastName}   <span class="comment-date">${comment.dateTime}</span></h4>
                <h4 class="comment-header">${comment.header}</h4>
                <p class="comment-text">${comment.text}</p>
            </div>
        </c:forEach>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>