<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>

<div class="content">
    <%@include file="/WEB-INF/jspf/clientMenu.jspf"%>
    <div class="container">
        <form name="createAccountForm" method="POST" action="/controller" class="form-account" id="form-account">
            <input type="hidden" name="command" value="createAccount" />
            <h3 class="head"><fmt:message key="createAccount"/></h3>
            <div class="account-content">
                <label for="tariffs"><fmt:message key="nameTariff" /><span class="star">*</span></label>
                <select id="tariffs" name="tariffs" class="form-control">
                    <c:forEach var="tariff" items="${tariffs}">
                        <option value=${tariff.id}>${tariff.tariffName}</option>
                    </c:forEach>
                </select>
                <br/>
                <label for="address"><fmt:message key="accountAddress" /><span class="star">*</span></label>
                <input type="text" id="address" name="address" class="form-control" required>
                <fmt:message key="ok" var="buttonCreateAccount" />
                <input type="submit" value="${buttonCreateAccount}" class="btn btn-login-submit"/>
                <br/>
                <p class="result-label">${result}</p>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<%@include file="/WEB-INF/jspf/footer.jspf"%>

</body>
</html>

