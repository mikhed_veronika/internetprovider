<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<div class="content">
    <%@include file="/WEB-INF/jspf/clientMenu.jspf"%>
    <div class="container">
        <form name="payForm" method="POST" action="/controller" class="form-comment" id="form-comment">
            <input type="hidden" name="command" value="addComment" />
            <h3 class="head"><fmt:message key="addComment"/></h3>
            <div class="comment-form-content">
                <label for="comment-header"><fmt:message key="commentHeader"/><span class="star">*</span></label>
                <input type="text" id="comment-header" name="commentHeader" class="form-control" maxlength="150" required title="Not more than 150 symbols">
                <br/>
                <label for="comment-text"><fmt:message key="commentText"/><span class="star">*</span></label>
                <textarea rows="6" id="comment-text" name="commentText" class="form-control" required></textarea>
                <fmt:message key="sendBtn" var="send" />
                <input id="send" type="submit" value="${send}" class="btn btn-login-submit"/>
                <br/>
                <p class="result-label">${result}</p>
            </div>
        </form>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>

