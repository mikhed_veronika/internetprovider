<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="stylesheet" href="../../css/site.css">
</head>
<body>

<div class="content">
    <%@include file="/WEB-INF/jspf/clientMenu.jspf"%>
    <div class="content-main">
        <div class="jumbotron">
            <h2 class="header-main"><fmt:message key="starNet"/> </h2>
            <h1 class="header-main"><fmt:message key="welcome"/> ${user.firstName}!</h1>
            <br/>
            <h2 class="lead text-main-head"><fmt:message key="clientMain"/></h2> <br/>
            <ul class="text-client">
                <li><fmt:message key="clientMain1"/></li>
                <li><fmt:message key="clientMain2"/></li>
                <li><fmt:message key="clientMain3"/></li>
                <li><fmt:message key="clientMain4"/></li>
                <li><fmt:message key="clientMain5"/></li>
                <li><fmt:message key="clientMain6"/></li>
                <li><fmt:message key="clientMain7"/></li>
            </ul>
            <br/><br/>
            <a href="/controller?command=clientProfile" class="main-btn"><fmt:message key="clientMainProfile"/></a>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>

