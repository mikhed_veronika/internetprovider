<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>

<div class="content">
    <%@include file="/WEB-INF/jspf/clientMenu.jspf"%>
    <div class="container">
        <form name="payForm" method="POST" action="/controller" class="form-payment" id="form-payment">
            <input type="hidden" name="command" value="pay" />
            <h3 class="head"><fmt:message key="makePayment"/></h3>
            <div class="payment-content">
                <label for="accountId"><fmt:message key="accountID" /><span class="star">*</span></label>
                <select id="accountId" name="accountId" class="form-control" required>
                    <c:forEach var="account" items="${accounts}">
                        <option value=${account.id}><fmt:message key="contract"/> ${account.id} (${account.tariffName})</option>
                    </c:forEach>
                </select>
                <br/>
                <label for="summa"><fmt:message key="paymentSum" /><span class="star">*</span></label>
                <input type="number" min="0" max="1000000" id="summa" name="summa" class="form-control" required title = Max value is 1000000>
                <br/>
                <label for="card"><fmt:message key="card"/><span class="star">*</span></label>
                <input type="text" id="card" name="card" class="form-control" placeholder="XXXXXXXXXXXXXXXX" pattern="[0-9]{16}" title = "16 digits" required>
                <br/>
                <label for="cvv2"><fmt:message key="cvv2"/><span class="star">*</span></label>
                <input type="text" id="cvv2" name="cvv2" class="form-control" placeholder="XXX" pattern="[0-9]{3}" title = "3 digits" required>
                <br/>
                <input id="checkAgree" type="checkbox" name="check">
                <label for="checkAgree"><fmt:message key="checkBoxConfirm"/></label>
                <fmt:message key="ok" var="buttonPay"/>
                <input id="submit-pay" type="submit" disabled value="${buttonPay}" class="btn btn-login-submit"/>
                <br/>
                <p class="result-label">${result}</p>
            </div>
        </form>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/script-payment.js"></script>
</body>
</html>

