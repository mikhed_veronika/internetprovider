<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>

<div class="content">
    <%@include file="/WEB-INF/jspf/clientMenu.jspf"%>
    <div class="tariffs-container">
        <br/>
        <h3 class="header"><fmt:message key="archiveTariffs"/></h3>
        <br/>
        <c:forEach var="tariff" items="${tariffs}">
            <div class="table-block">
                <table class="table table-tariffs">
                    <thead>
                    <tr><td class="tariff-name" colspan="2">${tariff.tariffName}</td></tr>
                    </thead>
                    <tbody>
                    <c:if test="${tariff.description!=null && tariff.description.length()!=0}">
                        <tr>
                            <td><fmt:message key="tariffDescription"/></td>
                            <td>${tariff.description}</td>
                        </tr>
                    </c:if>
                    <tr>
                        <td><fmt:message key="monthPayment"/></td>
                        <td><fmt:formatNumber maxFractionDigits="0" value="${tariff.monthPayment}"/>  ${tariff.currencyCode}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="uploadSpeed"/></td>
                        <td>${tariff.uploadSpeed}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="downloadSpeed"/></td>
                        <td>${tariff.downloadSpeed}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="trafficVolume"/></td>
                        <td>
                            <c:if test="${tariff.trafficVolume < 0}">
                                <c:out value="∞"/>
                            </c:if>
                            <c:if test="${tariff.trafficVolume >= 0}">
                                <c:out value="${tariff.trafficVolume}"/>
                            </c:if>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </c:forEach>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>

