<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
    <div class="content">
        <%@include file="/WEB-INF/jspf/clientMenu.jspf"%>
        <div class="container">
            <form name="closeAccountForm" method="POST" action="/controller" class="form-close" id="form-close">
                <input type="hidden" name="command" value="closeAccount" />
                <h3 class="head"><fmt:message key="closeAccount"/></h3>
                <div class="close-content">
                    <label for="accountId"><fmt:message key="closeLabel"/><span class="star">*</span></label>
                    <select id="accountId" name="accountId" class="form-control" required>
                        <c:forEach var="account" items="${accounts}">
                            <option value=${account.id}><fmt:message key="contract"/> ${account.id} (${account.tariffName})</option>
                        </c:forEach>
                    </select>
                    <fmt:message key="ok" var="buttonClose" />
                    <input id="submit-close" type="submit" value="${buttonClose}" class="btn btn-login-submit"/>
                    <br/>
                    <p class="result-label">${result}</p>
                </div>
            </form>
        </div>
    </div>
    <%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>

