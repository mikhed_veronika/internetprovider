<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>

<div class="content">
<%@include file="/WEB-INF/jspf/clientMenu.jspf"%>
    <nav class="navbar profile-nav">
        <a href="/controller?command=redirectToChangePassword" class="ref-profile"><fmt:message key="changePassword"/></a>
        <a href="/controller?command=redirectToCloseAccount" class="ref-profile"><fmt:message key="closeAccount"/></a>
        <a href="/controller?command=redirectToCreatingAccount" class="ref-profile"><fmt:message key="createAccount"/></a>
        <a href="/controller?command=redirectToPayment" class="ref-profile"><fmt:message key="makePayment"/></a>
    </nav>

    <div class="container">
        <ul class="nav nav-tabs client-menu">
            <li><a href="#info" class="active" data-toggle="tab"><fmt:message key="information"/></a></li>
            <li><a href="#accounts" data-toggle="tab"><fmt:message key="accounts"/></a></li>
            <li><a href="#payments" data-toggle="tab"><fmt:message key="paymentsHistory"/> </a></li>
            <li><a href="#tariffAddRequests" data-toggle="tab"><fmt:message key="requestsList"/> </a></li>
        </ul>

        <div class="tab-content client-container">
            <div id="info" class="tab-pane fade in active">
                <h3 class="title-profile"><fmt:message key="information"/></h3>
                <ul class="profile-info">
                    <li><span class="span-label"><fmt:message key="firstNameLabel"/></span><span class="span-content">${user.firstName}</span></li>
                    <li><span class="span-label"><fmt:message key="lastNameLabel"/></span><span class="span-content">${user.lastName}</span></li>
                    <li><span class="span-label"><fmt:message key="passportLabel"/></span><span class="span-content">${user.passport}</span></li>
                    <li><span class="span-label"><fmt:message key="emailLabel"/></span><span class="span-content">${user.email}</span></li>
                    <li><span class="span-label"><fmt:message key="phoneLabel"/></span><span class="span-content">${user.phoneNumber}</span></li>
                    <li><span class="span-label"><fmt:message key="addressLabel"/></span><span class="span-content">${user.address}</span></li>
                </ul>
            </div>

            <div id="accounts" class="tab-pane fade">
                <div class="client-container">
                    <h3 class="title-profile"><fmt:message key="accounts"/></h3>
                    <div class="panel-group " id="accordion">
                        <c:forEach var="account" items="${accounts}">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" class="collapse-header account-accordion"><fmt:message key="abonentContract"/> ${account.id}</a>
                                    </h4>
                                </div>
                                <div class="collapse-content panel-collapse collapse account-accordion">
                                    <div class="panel-body">
                                        <ul class="profile-info">
                                            <li><span class="account-span-label"><fmt:message key="accountID"/></span><span class="account-span-content">${account.id}</span></li>
                                            <li><span class="account-span-label"><fmt:message key="tariffName"/></span><span class="account-span-content">${account.tariffName}</span></li>
                                            <li><span class="account-span-label"><fmt:message key="monthPayment"/></span><span class="account-span-content"><fmt:formatNumber maxFractionDigits="0" value="${account.abonentPayment}"/></span></li>
                                            <li><span class="account-span-label"><fmt:message key="creatingDate"/></span><span class="account-span-content">${account.creatingDate}</span></li>
                                            <c:if test="${account.endDate!=null}">
                                                <li><span class="account-span-label"><fmt:message key="endDate"/></span><span class="account-span-content">${account.endDate}</span></li>
                                            </c:if>
                                            <li><span class="account-span-label"><fmt:message key="balance"/></span><span class="account-span-content"><fmt:formatNumber maxFractionDigits="0" value="${account.balance}"/></span></li>
                                            <li><span class="account-span-label"><fmt:message key="accountAddress"/></span><span class="account-span-content">${account.address}</span></li>
                                            <c:if test="${account.ban==true}">
                                                <li><span class="account-span-label"><fmt:message key="accountStatus"/></span><span class="account-span-content text-red"><fmt:message key="ban"/></span></li>
                                            </c:if>
                                            <c:if test="${account.ban==false}">
                                                <li><span class="account-span-label"><fmt:message key="accountStatus"/></span><span class="account-span-content text-green"><fmt:message key="notBan"/></span></li>
                                            </c:if>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>

            <div id="payments" class="tab-pane fade">
                <div class="client-container">
                    <h3 class="title-profile"><fmt:message key="paymentsHistory"/></h3>
                    <div class="panel-group " id="accordion-payments">
                        <c:forEach var="paymentList" items="${allPayments}">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion-payments" class="collapse-header payment-accordion"><fmt:message key="abonentContract"/> ${paymentList.accountId}</a>
                                    </h4>
                                </div>
                                <div class="collapse-content panel-collapse collapse payment-accordion">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th><fmt:message key="paymentDate"/></th>
                                                <th><fmt:message key="paymentSum"/></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach var="payment" items="${paymentList.paymentsList}">
                                                <tr>
                                                    <td>${payment.paymentDate}</td>
                                                    <td><fmt:formatNumber maxFractionDigits="0" value="${payment.paymentSum}"/></td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>

            <div id="tariffAddRequests" class="tab-pane fade">
                <div class="client-container">
                    <h3 class="title-profile"><fmt:message key="tariffAddRequests"/></h3>
                    <div class="client-requests-list">
                        <table class="table table-requests">
                            <c:if test="${tariffAddRequests.size()==0}">
                                <h4><fmt:message key="noRequests"/></h4>
                            </c:if>
                            <c:if test="${tariffAddRequests.size()!=0}">
                                <thead>
                                    <tr>
                                        <th><fmt:message key="nameTariff"/></th>
                                        <th><fmt:message key="address"/></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="tariffAddRequest" items="${tariffAddRequests}">
                                        <tr>
                                            <td>${tariffAddRequest.tariffName}</td>
                                            <td>${tariffAddRequest.address}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<%@include file="/WEB-INF/jspf/footer.jspf"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/script-client-profile.js"></script>

</body>
</html>

