<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<div class="content">
    <%@include file="/WEB-INF/jspf/clientMenu.jspf"%>
    <div class="container">
        <form name="changePasswordForm" method="POST" action="/controller" class="form-pass" id="form-pass" onsubmit="confirmPass(); return false">
            <input type="hidden" name="command" value="changePassword" />
            <h3 class="head"><fmt:message key="changePassword"/></h3>
            <div class="pass-content">
                <label for="currentPassword"><fmt:message key="currentPass"/><span class="star">*</span></label>
                <input type="password" id="currentPassword" name="currentPassword" class="form-control"
                       pattern=".{6,}" title="Six or more characters"  required>
                <br/>
                <label for="newPassword"><fmt:message key="newPass"/><span class="star">*</span></label>
                <input type="password" id="newPassword" name="newPassword" class="form-control"
                       pattern=".{6,}" title="Six or more characters"  required>
                <br/>
                <label for="newPasswordConfirm"><fmt:message key="newPassConfirm"/><span class="star">*</span></label>
                <input type="password" id="newPasswordConfirm" name="newPasswordConfirm" class="form-control"
                       pattern=".{6,}" title="Six or more characters"  required>
                <fmt:message key="ok" var="btnChangePass"/>
                <input type="submit" value="${btnChangePass}" class="btn btn-login-submit"/>
                <br/>
                <p class="result-label">${result}</p>
            </div>
        </form>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/script-change-password.js"></script>
</body>
</html>