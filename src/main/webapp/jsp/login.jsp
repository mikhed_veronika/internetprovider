<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="authenticationHeader"/></title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontello.css">
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>

<div class="content">
    <nav class="navbar nav-color">
        <ul class="nav navbar-nav nav-top">
            <li><a href="/controller?command=main" class="text-main"><fmt:message key="starNet"/></a></li>
            <li><a href="/controller?command=tariffs" class="text-menu"><fmt:message key="tariffs"/></a></li>
            <li><a href="/controller?command=comments" class="text-menu"><fmt:message key="comments"/></a></li>
        </ul>
        <form class="lang-select">
            <select id="language" name="language" onchange="submit()" class="form-control">
                <option value="ru" ${language == 'ru' ? 'selected' : ''}><fmt:message key="ru"/> </option>
                <option value="en" ${language == 'en' ? 'selected' : ''}><fmt:message key="en"/> </option>
            </select>
        </form>
        <a href="/controller?command=redirectToLogin" class="btn btn-ref"><span class="icon-user"></span><fmt:message key="login"/></a>
    </nav>

    <button type="button" name="back" onclick="history.back()" class="request-btn btn button-back"><fmt:message key="back"/></button>

    <div class = "container">
        <form name="loginForm" method="POST" action="/controller" class="form-reg" id="form-login">
            <input type="hidden" name="command" value="login" />
            <h3 class="head"><fmt:message key="authenticationHeader"/></h3>
            <div class="login-content">
                <label for="email1"><fmt:message key="emailLabel"/></label>
                <input type="email" id="email1" name="email" required class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}">
                <br/>
                <label for="password1"><fmt:message key="passwordLabel" /></label>
                <input type="password" id="password1" name="password" required class="form-control" pattern=".{6,}" title="Six or more characters">
                <fmt:message key="login" var="buttonLogin"/>
                <input type="submit" value="${buttonLogin}" class="btn btn-login-submit"/>
                <p id="reg-link" class="link"><fmt:message key="register"/></p>
                <br/>
                <p class="result-label">${result}</p>
            </div>
        </form>

        <form name="regForm" method="POST" action="/controller" class="form-reg" id="form-register">
            <h3 class="head"><fmt:message key="registerHeader"/></h3>
            <input type="hidden" name="command" value="register" />
            <div class="login-content">
                <div class="form-group-inline">
                    <label for="first-name"><fmt:message key="firstNameLabel" /><span class="star">*</span></label>
                    <input type="text" id="first-name" name="first-name" class="form-control input" pattern="[^\s\d]{2,}" title = "Two or more letters" required>
                </div>
                <div class="form-group-inline">
                    <label for="last-name"><fmt:message key="lastNameLabel" /><span class="star">*</span></label>
                    <input type="text" id="last-name" name="last-name" class="form-control input" pattern="[^\s\d]{2,}" title = "Two or more letters" required>
                </div>
                <div class="form-group-inline">
                    <label for="passport"><fmt:message key="passportLabel" /><span class="star">*</span></label>
                    <input type="text" id="passport" name="passport" class="form-control input" pattern="[A-Z][A-Z]([0-9]){7}" title = "Two letters in upper case and seven digits" required>
                </div>
                <div class="form-group-inline">
                    <label for="address"><fmt:message key="addressLabel" /><span class="star">*</span></label>
                    <input type="text" id="address" name="address" class="form-control input" required>
                </div>
                <div class="form-group-inline">
                    <label for="phone"><fmt:message key="phoneLabel" /><span class="star">*</span></label>
                    <input type="text" id="phone" name="phone" class="form-control input" pattern="(\+)?[0-9]{7,13}" required>
                </div>
                <div class="form-group-inline">
                    <label for="email2"><fmt:message key="emailLabel"/><span class="star">*</span></label>
                    <input type="email" id="email2" name="email" class="form-control input"
                           pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                </div>
                <div class="form-group-inline">
                    <label for="password2"><fmt:message key="passwordLabel"/><span class="star">*</span></label>
                    <input type="password" id="password2" name="password" class="form-control input" pattern=".{6,}" title="Six or more characters"  required>
                </div>
                <fmt:message key="register" var="buttonRegister" />
                <input type="submit" value="${buttonRegister}" class="btn btn-login-submit"/>
                <p id="login-link" class="link"><fmt:message key="login"/></p>
                <br/>
                <p class="result-label">${result}</p>
            </div>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/script-login.js"></script>

</body>
</html>