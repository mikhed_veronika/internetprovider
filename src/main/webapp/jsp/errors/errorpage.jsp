<%@ page isErrorPage="true" import="java.io.*" contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setBundle basename="text"/>
<html>
<head>
    <title><fmt:message key="error" /></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
    <body>

        <%=exception.getMessage()%>
        StackTrace:
        <%
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            exception.printStackTrace(printWriter);
            printWriter.println(stringWriter);
            printWriter.close();
            stringWriter.close();
        %>
        <div class="back">
            <button type="button" name="back" onclick="history.back()" class="btn btn-info back-btn"><fmt:message key="back"/></button>
        </div>

    </body>
</html>


