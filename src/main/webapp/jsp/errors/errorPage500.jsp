<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isErrorPage="true" isELIgnored="false" %>
<% response.setStatus(500); %>
<html>
<head>
    <title>500 error</title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<div class="container">
    <h1 class="err-head">500 Internal Server Error</h1>
    <button type="button" name="back" onclick="history.back()" class="main-btn err-back">Back</button>
</div>
</body>
</html>