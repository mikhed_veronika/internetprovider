<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isErrorPage="true" isELIgnored="false" %>
<% response.setStatus(404); %>
<html>
<head>
    <title>404 error</title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<div class="container">
    <h1 class="err-head">404 Not found</h1>
    <button type="button" name="back" onclick="history.back()" class="main-btn err-back">Back</button>
</div>
</body>
</html>