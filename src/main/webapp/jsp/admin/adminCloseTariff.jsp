<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<div class="content">
    <%@include file="/WEB-INF/jspf/adminMenu.jspf"%>
    <div class="container">
        <form name="newTariffForm" method="POST" action="/controller" class="form-tariff" id="form-tariff">
            <input type="hidden" name="command" value="closeTariff" />
            <h3 class="head"><fmt:message key="closeTariff"/></h3>
            <div class="tariff-form-content">
                <label for="tariffs"><fmt:message key="nameTariff"/><span class="star">*</span></label>
                <select id="tariffs" name="tariffId" class="form-control" required>
                    <c:forEach var="tariff" items="${tariffs}">
                        <option value=${tariff.id}>${tariff.tariffName}</option>
                    </c:forEach>
                </select>
                <br/>
                <fmt:message key="ok" var="ok" />
                <input id="send" type="submit" value="${ok}" class="btn btn-login-submit"/>
                <br/>
                <p class="result-label">${result}</p>
            </div>
        </form>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>

