<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<div class="content">
    <%@include file="/WEB-INF/jspf/adminMenu.jspf"%>
    <div class="container ban-content">
        <h3 class="ban-header"><fmt:message key="setBan"/></h3><br/><br/>
        <form name="banForm" method="POST" action="/controller" class="form-ban" id="form-ban">
            <input type="hidden" name="command" value="setBan" />
            <table class="table">
                <thead>
                <tr>
                    <th><fmt:message key="clientID"/></th>
                    <th><fmt:message key="accountID"/></th>
                    <th><fmt:message key="nameTariff"/></th>
                    <th><fmt:message key="addressLabel"/></th>
                    <th><fmt:message key="dateStart"/></th>
                    <th><fmt:message key="balanceSum"/></th>
                    <th><fmt:message key="select"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="setBanAccount" items="${setBanAccounts}">
                    <tr>
                        <td>${setBanAccount.clientId}</td>
                        <td>${setBanAccount.id}</td>
                        <td>${setBanAccount.tariffName}</td>
                        <td>${setBanAccount.address}</td>
                        <td>${setBanAccount.creatingDate}</td>
                        <td><fmt:formatNumber maxFractionDigits="0" value="${setBanAccount.balance}"/></td>
                        <td><input type="checkbox" name="setBanId" value=${setBanAccount.id}></td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="7"><br/><button class="request-btn btn btn-danger" type="submit"><fmt:message key="setBan"/></button></td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>
</html>

