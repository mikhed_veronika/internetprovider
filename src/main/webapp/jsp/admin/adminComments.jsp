<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="comments"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>

<body>
<div class="content">
    <%@include file="/WEB-INF/jspf/adminMenu.jspf"%>
    <div class="comments-container">
        <br/>
        <h3 class="header"><fmt:message key="comments"/></h3> <br/>
        <form name="deleteCommentsForm" method="POST" action="/controller" class="form-tariff-requests" id="form-tariff-requests">
            <input type="hidden" name="command" value="deleteComments"/>
            <button class="request-btn btn btn-danger delete-comment" type="submit"><fmt:message key="delete"/></button>
            <c:forEach var="comment" items="${comments}">
                <div class="comment-block removable">
                    <input class="checkbox-remove" type="checkbox" name="deleteComment" value=${comment.id}>
                    <h4 class="comment-sender">${comment.firstName} ${comment.lastName} <span class="comment-date">${comment.dateTime}</span></h4>
                    <h4 class="comment-header">${comment.header}</h4>
                    <p class="comment-text">${comment.text}</p>
                </div>
            </c:forEach>
        </form>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>