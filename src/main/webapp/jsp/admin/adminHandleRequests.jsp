<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>

<body>
<div class="content">
    <%@include file="/WEB-INF/jspf/adminMenu.jspf"%>
    <div class="container">
        <br/>
        <h3><fmt:message key="requestsConnect"/></h3>
        <br/>
        <form name="newTariffForm" method="POST" action="/controller" class="form-tariff-requests" id="form-tariff-requests">
            <input type="hidden" name="command" value="handleRequestAddTariff" />
            <table class="table">
                <thead>
                    <tr>
                        <th><fmt:message key="clientID"/></th>
                        <th><fmt:message key="nameTariff"/></th>
                        <th><fmt:message key="addressLabel"/></th>
                        <th><fmt:message key="select"/></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="tariffAddRequest" items="${tariffAddRequests}">
                        <tr>
                            <td>${tariffAddRequest.clientId}</td>
                            <td>${tariffAddRequest.tariffName}</td>
                            <td>${tariffAddRequest.address}</td>
                            <td><input type="checkbox" name="requestAddTariff" value=${tariffAddRequest.id}></td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td colspan="4">
                            <button class="request-btn btn btn-success" type="submit" name="requestAction" value="addTariff"><fmt:message key="requestAdd"/></button>
                            <button class="request-btn btn btn-danger" type="submit" name="requestAction" value="dontAddTariff"><fmt:message key="delete"/></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>

