<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<div class="content">
    <%@include file="/WEB-INF/jspf/adminMenu.jspf"%>
    <div class="container">
        <form name="newTariffForm" method="POST" action="/controller" class="form-tariff" id="form-tariff">
            <input type="hidden" name="command" value="addTariff" />
            <h3 class="head"><fmt:message key="addTariffHeader"/></h3>
            <div class="tariff-form-content">
                <label for="tariff-name"><fmt:message key="addTariffName"/><span class="star">*</span></label>
                <input type="text" id="tariff-name" name="tariffName" class="form-control" maxlength="255" required title="Not more than 255 symbols">
                <br/>
                <label for="tariff-description"><fmt:message key="addTariffDescription"/><span class="star">*</span></label>
                <textarea rows="4" id="tariff-description" name="tariffDescription" class="form-control"></textarea>
                <br/>
                <label for="month-payment"><fmt:message key="addTariffPayment"/><span class="star">*</span></label>
                <input type="number" id="month-payment" name="monthPayment" class="form-control" min="0" required title="Value must be >= 0">
                <br/>
                <label for="currency-code"><fmt:message key="addTariffCurrencyCode"/><span class="star">*</span></label>
                <select id="currency-code" name="currencyCode" class="form-control" required>
                    <option value="BYR"><fmt:message key="currencyBel"/></option>
                </select>
                <br/>
                <label for="upload-speed"><fmt:message key="addTariffUploadSpeed"/><span class="star">*</span></label>
                <input type="number" id="upload-speed" name="uploadSpeed" class="form-control" min="0" max="10000" required title="Value must be >= 0">
                <br/>
                <label for="download-speed"><fmt:message key="addTariffDownloadSpeed"/></label>
                <input type="number" id="download-speed" name="downloadSpeed" class="form-control" min="0" max = "10000" required title="Value must be >= 0">
                <br/>
                <label for="traffic-volume"><fmt:message key="addTariffTrafficVolume"/><span class="star">*</span></label>
                <input type="number" id="traffic-volume" name="trafficVolume" class="form-control" required title="Number < 0 means unlimit traffic volume" >
                <br/>
                <fmt:message key="ok" var="ok" />
                <input id="send" type="submit" value="${ok}" class="btn btn-login-submit"/>
                <br/>
                <p class="result-label">${result}</p>
            </div>
        </form>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>

