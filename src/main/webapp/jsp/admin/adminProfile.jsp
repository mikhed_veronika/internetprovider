<%@ page pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<html lang="${language}">
<head>
    <title><fmt:message key="starNet"/></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/fontello.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<div class="content">
    <%@include file="/WEB-INF/jspf/adminMenu.jspf"%>
    <nav class="navbar">
        <a href="/controller?command=redirectToChangePassword" class="ref-profile"><fmt:message key="changePassword"/></a>
        <a href="/controller?command=redirectToCloseTariff" class="ref-profile"><fmt:message key="closeTariff"/></a>
        <a href="/controller?command=redirectToChangeTariff" class="ref-profile"><fmt:message key="changeTariff"/></a>
        <a href="/controller?command=redirectToAddTariff" class="ref-profile"><fmt:message key="addTariffHeader"/></a>
        <a href="/controller?command=redirectToHandleRequests" class="ref-profile"><fmt:message key="requestsConnect"/></a>
        <a href="/controller?command=redirectToSetBan" class="ref-profile"><fmt:message key="setBan"/></a>
        <a href="/controller?command=redirectToSetUnban" class="ref-profile"><fmt:message key="setUnban"/></a>
    </nav>
    <div class="container">
        <div id="info">
            <h3 class="title-profile"><fmt:message key="information"/></h3>
            <ul class="profile-info">
                <li><span class="span-label"><fmt:message key="firstNameLabel"/></span><span class="span-content">${user.firstName}</span></li>
                <li><span class="span-label"><fmt:message key="lastNameLabel"/></span><span class="span-content">${user.lastName}</span></li>
                <li><span class="span-label"><fmt:message key="passportLabel"/></span><span class="span-content">${user.passport}</span></li>
                <li><span class="span-label"><fmt:message key="emailLabel"/></span><span class="span-content">${user.email}</span></li>
                <li><span class="span-label"><fmt:message key="phoneLabel"/></span><span class="span-content">${user.phoneNumber}</span></li>
                <li><span class="span-label"><fmt:message key="addressLabel"/></span><span class="span-content">${user.address}</span></li>
            </ul>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>

