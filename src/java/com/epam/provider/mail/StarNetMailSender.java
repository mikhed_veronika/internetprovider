package com.epam.provider.mail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Provides sending messages to clients
 * @author Veronika Mikhed
 */

public class StarNetMailSender {

    private final String EMAIL = "provider.starnet@gmail.com";
    private final String PASSWORD = "20521254q";

    public static final String REGISTER_SUCCESS_MESSAGE = "Dear client! You are successfully registered at system of internet provider StarNet. Thank you for using our provider!";

    private Properties properties;
    private Session session;

    public StarNetMailSender() {
        properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.debug", "true");

        session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(EMAIL, PASSWORD);
                    }
                });
    }

    public void send(String to, String text) throws MessagingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(EMAIL));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(to));
        message.setSubject("StarNet");
        message.setText(text);

        Transport.send(message);
    }

}
