package com.epam.provider.command;


import com.epam.provider.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command that invokes in case of any error. It redirects users to login page.
 * @author Veronika Mikhed
 */

public class NoCommand implements Command{
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        request.setAttribute(ATTR_PAGE, REDIRECT_PAGE);
        return page;
    }
}
