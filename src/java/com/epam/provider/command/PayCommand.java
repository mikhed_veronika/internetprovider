package com.epam.provider.command;

import com.epam.provider.entity.Account;
import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.AccountService;
import com.epam.provider.service.PaymentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

/**
 * Command that provides increasing balance of account.
 * This command can be used only by clients.
 * @author Veronika Mikhed
 */

public class PayCommand implements Command {
    public static final Logger LOG = Logger.getLogger(PayCommand.class);
    private AccountService accountService;
    private PaymentService paymentService;

    private static final String PARAM_NAME_ACCOUNTS = "accounts";
    private static final String PARAM_NAME_ACCOUNT_ID = "accountId";
    private static final String PARAM_NAME_SUMMA = "summa";
    private static final String PARAM_NAME_CARD = "card";
    private static final String PARAM_NAME_CVV2 = "cvv2";

    public static final String CARD_NUMBER_REGEX = "[0-9]{16}";
    public static final String CVV2_REGEX = "[0-9]{3}";

    public PayCommand() {
        accountService = new AccountService();
        paymentService = new PaymentService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        User user = (User)request.getSession().getAttribute("user");
        boolean res = false;
        int accountId = Integer.valueOf(request.getParameter(PARAM_NAME_ACCOUNT_ID));
        BigDecimal summa = new BigDecimal(request.getParameter(PARAM_NAME_SUMMA));
        String cardNumber = request.getParameter(PARAM_NAME_CARD);
        String cvv2 = request.getParameter(PARAM_NAME_CVV2);

        List<Account> accounts = null;

        if (user==null || user.getRole()!= Role.CLIENT) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            if (checkInfo(summa, cardNumber, cvv2)) {
                try {
                    res = paymentService.addMoneyToAccount(accountId, summa, cardNumber, cvv2);
                    accounts = accountService.takeActiveAccountsByClientId(user.getId());
                } catch (ServiceException e) {
                    LOG.error("Error in payCommand");
                }
            }
            if (res) {
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_PAYMENT_PAGE_PATH);
                request.setAttribute(PARAM_NAME_ACCOUNTS, accounts);
                request.setAttribute(ATTR_RESULT, SUCCESS_RESULT);
            }
            else {
                request.setAttribute(ATTR_RESULT, ERROR_RESULT);
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_PAYMENT_PAGE_PATH);
            }
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }

    private boolean checkInfo (BigDecimal summa, String cardNumber, String cvv2) {
        boolean res = false;
        if (summa.compareTo(new BigDecimal("0"))>0 && summa.compareTo(new BigDecimal("1000000"))<=0
                && cardNumber.matches(CARD_NUMBER_REGEX) && cvv2.matches(CVV2_REGEX)) {
            res = true;
        }
        return res;
    }
}
