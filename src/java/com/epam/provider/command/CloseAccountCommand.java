package com.epam.provider.command;


import com.epam.provider.entity.Account;
import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.AccountService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that closes account of client.
 * This command can be used only by client.
 * @author Veronika Mikhed
 */

public class CloseAccountCommand implements Command{

    public static final Logger LOG = Logger.getLogger(CloseAccountCommand.class);
    private AccountService accountService;

    private static final String PARAM_NAME_ACCOUNTS = "accounts";
    private static final String PARAM_NAME_ACCOUNT_ID = "accountId";
    private static final String BAD_RESULT="Can not delete account. Check your balance.";

    public CloseAccountCommand() {
        accountService = new AccountService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        boolean res = false;
        User user = (User)request.getSession().getAttribute("user");
        int accountId = Integer.valueOf(request.getParameter(PARAM_NAME_ACCOUNT_ID));
        List<Account> accounts = new ArrayList<Account>();
        if (user==null || user.getRole()!= Role.CLIENT) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            try {
                accounts = accountService.takeActiveAccountsByClientId(user.getId());
                for(int i = 0; i<accounts.size(); i++) {
                    if (accounts.get(i).getId()==accountId &&  (accounts.get(i).getBalance().compareTo(BigDecimal.ZERO) >= 0)) {
                        res = accountService.closeAccount(accountId);
                        break;
                    }
                }
                accounts=accountService.takeActiveAccountsByClientId(user.getId());
            } catch (ServiceException e) {
                LOG.error("Error in closeAccountCommand");
            }
            if (res) {
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_CLOSE_ACCOUNT_PAGE_PATH);
                request.setAttribute(PARAM_NAME_ACCOUNTS, accounts);
                request.setAttribute(ATTR_RESULT, SUCCESS_RESULT);
            }
            else {
                request.setAttribute(ATTR_RESULT, BAD_RESULT);
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_CLOSE_ACCOUNT_PAGE_PATH);
            }
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
