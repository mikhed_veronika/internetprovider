package com.epam.provider.command;

import com.epam.provider.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Command that invalidates current session.
 * This command can be used only by authorized users.
 * @author Veronika Mikhed
 */

public class LogoutCommand implements Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession currentSession = request.getSession(false);
        if (currentSession != null) {
            currentSession.invalidate();
        }
        String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_PAGE_PATH);
        request.setAttribute(ATTR_PAGE, REDIRECT_PAGE);
        return page;
    }
}