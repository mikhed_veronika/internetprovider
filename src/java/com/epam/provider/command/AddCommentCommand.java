package com.epam.provider.command;

import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.CommentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command that adds client's comment about provider.
 * This command can be used only by client.
 * @author Veronika Mikhed
 */

public class AddCommentCommand implements Command{

    public static final Logger LOG = Logger.getLogger(AddCommentCommand.class);

    public static final String PARAM_NAME_COMMENT_HEADER = "commentHeader";
    public static final String PARAM_NAME_COMMENT_TEXT = "commentText";

    private CommentService commentService;

    public AddCommentCommand() {
        commentService = new CommentService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        boolean res = false;
        User user = (User)request.getSession().getAttribute("user");
        String commentHeader = request.getParameter(PARAM_NAME_COMMENT_HEADER);
        String commentText = request.getParameter(PARAM_NAME_COMMENT_TEXT);
        if (user==null || user.getRole()!= Role.CLIENT) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            try {
                res = commentService.insertComment(user.getId(), commentHeader, commentText);
            } catch (ServiceException e) {
                LOG.error("Error in AddCommentCommand");
            }
            request.setAttribute(ATTR_RESULT, res ? SUCCESS_RESULT : ERROR_RESULT);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_ADD_COMMENT_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
