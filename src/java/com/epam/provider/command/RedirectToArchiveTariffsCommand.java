package com.epam.provider.command;

import com.epam.provider.entity.Tariff;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.TariffService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that redirects page with closed tariff plans.
 * This command can be used both by administrator and client.
 * @author Veronika Mikhed
 */

public class RedirectToArchiveTariffsCommand implements Command {

    public static Logger LOG = Logger.getLogger(RedirectToArchiveTariffsCommand.class);
    public static final String ATTR_TARIFFS = "tariffs";

    private TariffService tariffService;

    public RedirectToArchiveTariffsCommand() {
        tariffService = new TariffService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response)  {
        List<Tariff> tariffs = new ArrayList<Tariff>();
        String page;
        User user;
        user = (User)request.getSession().getAttribute("user");
        try {
            tariffs = tariffService.takeAllClosedTariffsList();
        } catch (ServiceException e) {
            LOG.error("Error while tariffs loading");
        }
        request.setAttribute(ATTR_TARIFFS, tariffs);
        if (user==null) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            switch (user.getRole()) {
                case CLIENT:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_ARCHIVE_TARIFFS_PAGE_PATH);
                    break;
                case ADMIN:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_ARCHIVE_TARIFFS_PAGE_PATH);
                    break;
                default:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
            }
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
