package com.epam.provider.command;

import com.epam.provider.entity.Account;
import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.AccountService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that unban selected users.
 * This command can be used only by administrator.
 * @author Veronika Mikhed
 */

public class SetUnbanCommand implements Command{

    public static final Logger LOG = Logger.getLogger(SetUnbanCommand.class);

    public static final String ATTR_SET_UNBAN_ACCOUNTS = "setUnbanAccounts";
    public static final String PARAM_NAME_SET_UNBAN = "setUnbanId";

    private AccountService accountService;

    public SetUnbanCommand() { accountService = new AccountService(); }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        User user = (User)request.getSession().getAttribute("user");
        String[] listToUnban;
        List<Account> accountsToUnban = new ArrayList<>();
        if (user==null || user.getRole()!= Role.ADMIN) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            listToUnban = request.getParameterValues(PARAM_NAME_SET_UNBAN);
            try {
                accountService.unbanUsersByAccountId(listToUnban);
                accountsToUnban = accountService.takeAccountsToUnban();
            } catch (ServiceException e) {
                LOG.error("Error in SetUnbanCommand " + e);
            }
            request.setAttribute(ATTR_SET_UNBAN_ACCOUNTS, accountsToUnban);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_SET_UNBAN_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
