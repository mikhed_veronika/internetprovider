package com.epam.provider.command;


import com.epam.provider.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command that redirects to login page.
 * This command can be used by all.
 * @author Veronika Mikhed
 */

public class RedirectToLoginPageCommand implements Command{
    public static final Logger LOG = Logger.getLogger(RedirectToLoginPageCommand.class);

    public RedirectToLoginPageCommand() { }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;
        page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        request.setAttribute(ATTR_PAGE, REDIRECT_PAGE);
        return page;
    }
}
