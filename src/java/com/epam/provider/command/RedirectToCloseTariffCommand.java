package com.epam.provider.command;


import com.epam.provider.entity.Role;
import com.epam.provider.entity.Tariff;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.TariffService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that redirects to close tariff plan page.
 * This command can be used only by administrator.
 * @author Veronika Mikhed
 */

public class RedirectToCloseTariffCommand implements Command {

    public static final Logger LOG = Logger.getLogger(RedirectToCloseTariffCommand.class);
    public static final String ATTR_NAME_TARIFFS = "tariffs";

    private TariffService tariffService;

    public RedirectToCloseTariffCommand() {
        tariffService = new TariffService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;
        User user = (User)request.getSession().getAttribute("user");
        List<Tariff> tariffs = new ArrayList<Tariff>();
        if (user!=null && user.getRole()== Role.ADMIN) {
            try {
                tariffs = tariffService.takeAllActiveTariffsList();
            } catch (ServiceException e) {
                LOG.error("Error while tariffs loading");
            }
            request.setAttribute(ATTR_NAME_TARIFFS, tariffs);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_CLOSE_TARIFF_PAGE_PATH);
        }
        else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
