package com.epam.provider.command;

import com.epam.provider.encryption.PasswordEncryption;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.mail.StarNetMailSender;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.UserService;
import org.apache.log4j.Logger;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command that authenticate user and redirect it to main page depending on it's role.
 * This command can be used by all.
 * @author Veronika Mikhed
 */

public class LoginCommand implements Command {

    public static final Logger LOG = Logger.getLogger(LoginCommand.class);
    private UserService userService;

    private static final String PARAM_NAME_EMAIL = "email";
    private static final String PARAM_NAME_PASSWORD = "password";

    private static final String ATTR_USER = "user";
    private static final String ATTR_INCORRECT = "Incorrect email or password";

    public LoginCommand() {
        userService = new UserService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        User user = null;
        String page;
        String email = request.getParameter(PARAM_NAME_EMAIL);
        String password = PasswordEncryption.md5(request.getParameter(PARAM_NAME_PASSWORD));
        try {
            user = userService.authenticate(email, password);
        } catch (ServiceException e) {
            LOG.error("Error authentication");
        }
        if (user!=null) {
            request.getSession().setAttribute(ATTR_USER, user);
            switch (user.getRole()) {
                case CLIENT:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_CLIENT_PAGE_PATH);
                    break;
                case ADMIN:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_ADMIN_PAGE_PATH);
                    break;
                default:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_PAGE_PATH);
            }
            request.setAttribute(ATTR_PAGE, REDIRECT_PAGE);
        } else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
            request.setAttribute(ATTR_RESULT, ATTR_INCORRECT);
            request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        }
        return page;
    }
}