package com.epam.provider.command;


import com.epam.provider.entity.Comment;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.CommentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Command that redirects users to page with reviews and comments.
 * This command can be used by guests, clients and administrators.
 * @author Veronika Mikhed
 */

public class CommentsCommand implements Command{
    public static final Logger LOG = Logger.getLogger(CommentsCommand.class);
    public static final String ATTR_NAME_COMMENTS = "comments";
    public static CommentService commentService;

    public CommentsCommand() {
        commentService = new CommentService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        List<Comment> comments = new ArrayList<Comment>();
        String page;
        User user = (User)request.getSession().getAttribute("user");
        try {
            comments = commentService.takeAllComments();
            Collections.reverse(comments);
        } catch (ServiceException e) {
            LOG.error("Error while comments loading");
        }

        request.setAttribute(ATTR_NAME_COMMENTS, comments);
        if (user==null) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.COMMENTS_PAGE_PATH);
        }
        else {
            switch (user.getRole()) {
                case CLIENT:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_COMMENTS_PAGE_PATH);
                    break;
                case ADMIN:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_COMMENTS_PAGE_PATH);
                    break;
                default:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.COMMENTS_PAGE_PATH);
            }
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
