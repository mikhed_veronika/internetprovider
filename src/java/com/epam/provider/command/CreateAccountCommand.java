package com.epam.provider.command;

import com.epam.provider.entity.Role;
import com.epam.provider.entity.Tariff;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.AccountService;
import com.epam.provider.service.TariffService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that sends request for connection to tariff plan.
 * This command can be used only by clients.
 * @author Veronika Mikhed
 */

public class CreateAccountCommand implements Command{

    public static final Logger LOG = Logger.getLogger(CreateAccountCommand.class);
    private AccountService accountService;
    private TariffService tariffService;
    private static final String PARAM_NAME_TARIFF_ID = "tariffs";
    public static final String ATTR_NAME_TARIFFS = "tariffs";
    private static final String PARAM_NAME_ADDRESS = "address";

    public CreateAccountCommand() {
        accountService = new AccountService();
        tariffService = new TariffService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        boolean res = false;
        User user = (User)request.getSession().getAttribute("user");
        List<Tariff> tariffs = new ArrayList<>();
        int tariffId = Integer.valueOf(request.getParameter(PARAM_NAME_TARIFF_ID));
        String address = request.getParameter(PARAM_NAME_ADDRESS);
        if (user==null || user.getRole()!= Role.CLIENT) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            try {
                tariffs = tariffService.takeAllActiveTariffsList();
                res = accountService.insertTariffAddRequest(user.getId(), tariffId, address);
            } catch (ServiceException e) {
                LOG.error("Error in createAccountCommand");
            }
            if (res && tariffs!=null) {
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_CREATING_ACCOUNT_PAGE_PATH);
                request.setAttribute(ATTR_RESULT, SUCCESS_RESULT);
                request.setAttribute(ATTR_NAME_TARIFFS, tariffs);
            }
            else {
                request.setAttribute(ATTR_RESULT, ERROR_RESULT);
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_CREATING_ACCOUNT_PAGE_PATH);
            }
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}