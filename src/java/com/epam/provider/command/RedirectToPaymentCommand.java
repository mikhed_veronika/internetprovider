package com.epam.provider.command;

import com.epam.provider.entity.Account;
import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.AccountService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that redirects to make payment page.
 * This command can be used only by client.
 * @author Veronika Mikhed
 */

public class RedirectToPaymentCommand implements Command{

    public static final Logger LOG = Logger.getLogger(RedirectToPaymentCommand.class);
    public static final String ATTR_NAME_ACCOUNTS = "accounts";
    private AccountService accountService;

    public RedirectToPaymentCommand() {
        accountService = new AccountService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;
        User user = (User)request.getSession().getAttribute("user");
        List<Account> accounts = new ArrayList<Account>();
        if (user!=null && user.getRole()== Role.CLIENT) {
            try {
                accounts = accountService.takeActiveAccountsByClientId(user.getId());
            } catch (ServiceException e) {
                LOG.error("Error while accounts loading");
            }
            request.setAttribute(ATTR_NAME_ACCOUNTS, accounts);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_PAYMENT_PAGE_PATH);
        }
        else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
