package com.epam.provider.command;


import com.epam.provider.entity.Role;
import com.epam.provider.entity.Tariff;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.TariffService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that closes a tariff plan.
 * This command can be used only by administrator.
 * @author Veronika Mikhed
 */

public class CloseTariffCommand implements Command{

    public static final Logger LOG = Logger.getLogger(CloseTariffCommand.class);
    public static final String PARAM_NAME_TARIFF_ID = "tariffId";
    public static final String ATTR_TARIFFS = "tariffs";

    private TariffService tariffService;

    public CloseTariffCommand() {
        tariffService = new TariffService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        boolean res = false;
        User user = (User)request.getSession().getAttribute("user");
        List<Tariff> tariffs = new ArrayList<Tariff>();
        int tariffId = Integer.valueOf(request.getParameter(PARAM_NAME_TARIFF_ID));
        if (user==null || user.getRole()!= Role.ADMIN) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            try {
                res = tariffService.closeTariffById(tariffId);
                tariffs = tariffService.takeAllActiveTariffsList();
            } catch (ServiceException e) {
                LOG.error("Error in CloseTariffCommand");
            }
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_CLOSE_TARIFF_PAGE_PATH);
            request.setAttribute(ATTR_TARIFFS, tariffs);
            request.setAttribute(ATTR_RESULT, res ? SUCCESS_RESULT : ERROR_RESULT);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
