package com.epam.provider.command;


import com.epam.provider.entity.Role;
import com.epam.provider.entity.Tariff;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.TariffService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that redirects to change tariff page.
 * This command can be used only by administrator.
 * @author Veronika Mikhed
 */

public class RedirectToChangeTariffCommand implements Command {
    public static final Logger LOG = Logger.getLogger(RedirectToChangeTariffCommand.class);

    public static final String ATTR_NAME_TARIFFS = "tariffs";

    private TariffService tariffService;

    public RedirectToChangeTariffCommand() {
        tariffService = new TariffService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;
        User user = (User)request.getSession().getAttribute("user");
        List<Tariff> tariffs = new ArrayList<Tariff>();
        if (user!=null && user.getRole()== Role.ADMIN) {
            try {
                tariffs = tariffService.takeAllActiveTariffsList();
            } catch (ServiceException e) {
                LOG.error("Error while tariff plans loading");
            }
            request.setAttribute(ATTR_NAME_TARIFFS, tariffs);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_CHANGE_TARIFF_PAGE_PATH);
        }
        else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
