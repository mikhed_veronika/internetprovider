package com.epam.provider.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Interface that contains only one method (execute) that returns page to be redirected
 * @author Veronika Mikhed
 */

public interface Command {

    String ATTR_PAGE = "toPage";
    String FORWARD_PAGE = "forward";
    String REDIRECT_PAGE = "redirect";
    String ATTR_RESULT = "result";
    String SUCCESS_RESULT = "Success";
    String ERROR_RESULT = "Error occured";


    String execute(HttpServletRequest request, HttpServletResponse response);
}