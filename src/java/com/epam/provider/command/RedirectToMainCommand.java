package com.epam.provider.command;

import com.epam.provider.entity.User;
import com.epam.provider.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command that redirects to main page.
 * This command can be used by all.
 * @author Veronika Mikhed
 */

public class RedirectToMainCommand implements Command{

    public static final Logger LOG = Logger.getLogger(RedirectToMainCommand.class);
    public static final String ATTR_USER = "user";

    public RedirectToMainCommand() {}

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        User user = null;
        String page;
        user = (User)request.getSession().getAttribute(ATTR_USER);
        if (user!=null) {
            switch (user.getRole()) {
                case ADMIN:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_ADMIN_PAGE_PATH);
                    break;
                case CLIENT:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_CLIENT_PAGE_PATH);
                    break;
                default:
                    page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_PAGE_PATH);
            }
        } else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, REDIRECT_PAGE);
        return page;
    }
}
