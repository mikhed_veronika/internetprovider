package com.epam.provider.command;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Class for storage commands.
 * @author Veronika Mikhed
 */

public class RequestHelper {

    private static RequestHelper instance = null;
    HashMap<String, Command> commands = new HashMap<String, Command>();

    private RequestHelper() {
        commands.put("redirectToLogin", new RedirectToLoginPageCommand());
        commands.put("main", new RedirectToMainCommand());
        commands.put("login", new LoginCommand());
        commands.put("register", new RegisterCommand());
        commands.put("tariffs", new TariffsCommand());
        commands.put("comments", new CommentsCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("clientProfile", new ClientProfileCommand());
        commands.put("adminProfile", new AdminProfileCommand());
        commands.put("redirectToCreatingAccount", new RedirectToCreatingAccountCommand());
        commands.put("createAccount", new CreateAccountCommand());
        commands.put("pay", new PayCommand());
        commands.put("redirectToPayment", new RedirectToPaymentCommand());
        commands.put("redirectToChangePassword", new RedirectToChangePasswordCommand());
        commands.put("changePassword", new ChangePasswordCommand());
        commands.put("redirectToAddComment", new RedirectToAddCommentCommand());
        commands.put("addComment", new AddCommentCommand());
        commands.put("redirectToAddTariff",  new RedirectToAddTariffCommand());
        commands.put("addTariff", new AddTariffCommand());
        commands.put("redirectToHandleRequests", new RedirectToHandleRequestsCommand());
        commands.put("handleRequestAddTariff", new HandleRequestAddTariffCommand());
        commands.put("redirectToCloseAccount", new RedirectToCloseAccountCommand());
        commands.put("closeAccount", new CloseAccountCommand());
        commands.put("deleteComments", new DeleteCommentsCommand());
        commands.put("redirectToSetBan", new RedirectToSetBanCommand());
        commands.put("setBan", new SetBanCommand());
        commands.put("redirectToSetUnban", new RedirectToSetUnbanCommand());
        commands.put("setUnban", new SetUnbanCommand());
        commands.put("redirectToCloseTariff", new RedirectToCloseTariffCommand());
        commands.put("closeTariff", new CloseTariffCommand());
        commands.put("redirectToChangeTariff", new RedirectToChangeTariffCommand());
        commands.put("changeTariff", new ChangeTariffCommand());
        commands.put("archiveTariffs", new RedirectToArchiveTariffsCommand());
    }

    public Command getCommand(HttpServletRequest request) {
        String action = request.getParameter("command");
        Command command = commands.get(action);
        if (command == null) {
            command = new NoCommand();
        }
        return command;
    }

    public static RequestHelper getInstance() {
        if (instance == null) {
            instance = new RequestHelper();
        }
        return instance;
    }
}
