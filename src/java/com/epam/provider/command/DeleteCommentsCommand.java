package com.epam.provider.command;


import com.epam.provider.entity.Comment;
import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.CommentService;
import org.apache.log4j.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that deletes selected commentaries.
 * This command can be used only by administrator.
 * @author Veronika Mikhed
 */

public class DeleteCommentsCommand implements Command {

    public static final Logger LOG = Logger.getLogger(DeleteCommentsCommand.class);
    public static final String PARAM_NAME_DELETE_COMMENT = "deleteComment";
    public static final String ATTR_NAME_COMMENTS = "comments";

    private CommentService commentService;

    public DeleteCommentsCommand() {
        commentService = new CommentService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        User user = (User)request.getSession().getAttribute("user");
        String[] deleteComment;
        List<Comment> comments = new ArrayList<>();
        if (user==null || user.getRole()!= Role.ADMIN) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            try {
                deleteComment = request.getParameterValues(PARAM_NAME_DELETE_COMMENT);
                if (deleteComment!=null) {
                    commentService.deleteComments(deleteComment);
                }
                comments = commentService.takeAllComments();
            } catch (ServiceException e) {
                LOG.error("Error in DeleteCommentsCommand " + e);
            }
            request.setAttribute(ATTR_NAME_COMMENTS, comments);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_COMMENTS_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
