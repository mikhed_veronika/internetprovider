package com.epam.provider.command;

import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.manager.ConfigurationManager;
import org.apache.log4j.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command that redirects to leave comment page.
 * This command can be used only by clients.
 * @author Veronika Mikhed
 */

public class RedirectToAddCommentCommand implements Command{

    public static final Logger LOG = Logger.getLogger(RedirectToAddCommentCommand.class);

    public RedirectToAddCommentCommand() { }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;
        User user = (User)request.getSession().getAttribute("user");
        if (user!=null && user.getRole()== Role.CLIENT) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_ADD_COMMENT_PAGE_PATH);
        }
        else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, REDIRECT_PAGE);
        return page;
    }
}
