package com.epam.provider.command;

import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command that redirects user to change password page.
 * This command can be used both by administrator and client.
 * @author Veronika Mikhed
 */

public class RedirectToChangePasswordCommand implements Command{

    public static final Logger LOG = Logger.getLogger(RedirectToChangePasswordCommand.class);

    public RedirectToChangePasswordCommand() { }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;
        User user = (User)request.getSession().getAttribute("user");
        if (user!=null) {
            if (user.getRole()== Role.ADMIN) {
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_CHANGE_PASSWORD_PAGE_PATH);
            }
            else {
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_CHANGE_PASSWORD_PAGE_PATH);
            }
        }
        else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, REDIRECT_PAGE);
        return page;
    }
}
