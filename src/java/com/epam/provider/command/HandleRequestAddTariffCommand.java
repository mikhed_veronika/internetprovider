package com.epam.provider.command;

import com.epam.provider.entity.Role;
import com.epam.provider.entity.TariffAddRequest;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.AccountService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that add new accounts by requests of users or delete requests for connection to tariffs.
 * This command can be used only by administrator.
 * @author Veronika Mikhed
 */

public class HandleRequestAddTariffCommand implements Command{

    public static final Logger LOG = Logger.getLogger(HandleRequestAddTariffCommand.class);
    public static final String ATTR_TARIFF_ADD_REQUESTS = "tariffAddRequests";
    public static final String PARAM_NAME_REQUEST_ACTION = "requestAction";
    public static final String PARAM_NAME_REQUEST = "requestAddTariff";
    public static final String PARAM_NAME_ADD_TARIFF = "addTariff";
    public static final String PARAM_NAME_DONT_ADD_TARIFF = "dontAddTariff";

    private AccountService accountService;

    public HandleRequestAddTariffCommand() {
        accountService = new AccountService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        String[] listRequests;
        User user = (User)request.getSession().getAttribute("user");
        String requestAction = request.getParameter(PARAM_NAME_REQUEST_ACTION);
        List<TariffAddRequest> tariffAddRequests = new ArrayList<>();
        if (user==null || user.getRole()!= Role.ADMIN) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            try {
                listRequests = request.getParameterValues(PARAM_NAME_REQUEST);
                if (listRequests!=null) {
                    if (PARAM_NAME_ADD_TARIFF.equals(requestAction)) {
                        accountService.createAccounts(listRequests);
                    }
                    if (PARAM_NAME_DONT_ADD_TARIFF.equals(requestAction)) {
                        accountService.deleteTariffAddRequests(listRequests);
                    }
                }
                tariffAddRequests = accountService.takeAllTariffAddRequests();
            } catch (ServiceException e) {
                LOG.error("Error in HandleRequestAddTariffCommand " + e);
            }
            request.setAttribute(ATTR_TARIFF_ADD_REQUESTS, tariffAddRequests);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_HANDLE_REQUESTS_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
