package com.epam.provider.command;

import com.epam.provider.entity.Account;
import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.AccountService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that set ban to selected clients.
 * This command can be used only by administrator.
 * @author Veronika Mikhed
 */

public class SetBanCommand implements Command{

    public static final Logger LOG = Logger.getLogger(SetBanCommand.class);

    public static final String ATTR_SET_BAN_ACCOUNTS = "setBanAccounts";
    public static final String PARAM_NAME_SET_BAN = "setBanId";

    private AccountService accountService;

    public SetBanCommand() {
        accountService = new AccountService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        User user = (User)request.getSession().getAttribute("user");
        String[] listToBan;
        List<Account> accountsToBan = new ArrayList<>();
        if (user==null || user.getRole()!= Role.ADMIN) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            listToBan = request.getParameterValues(PARAM_NAME_SET_BAN);
            try {
                accountService.banUsersByAccountId(listToBan);
                accountsToBan = accountService.takeAccountsToBan();
            } catch (ServiceException e) {
                LOG.error("Error in SetBanCommand " + e);
            }
            request.setAttribute(ATTR_SET_BAN_ACCOUNTS, accountsToBan);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_SET_BAN_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
