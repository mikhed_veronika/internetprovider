package com.epam.provider.command;

import com.epam.provider.entity.*;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.AccountService;
import com.epam.provider.service.PaymentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that redirects authorized users to client profile.
 * This command can be used only by client.
 * @author Veronika Mikhed
 */

public class ClientProfileCommand implements Command{

    public static final Logger LOG = Logger.getLogger(ClientProfileCommand.class);
    private AccountService accountService;
    private PaymentService paymentService;
    public static final String ATTR_ACCOUNTS = "accounts";
    public static final String ATTR_PAYMENTS = "allPayments";
    public static final String ATTR_REQUESTS = "tariffAddRequests";

    public ClientProfileCommand() {
        accountService = new AccountService();
        paymentService = new PaymentService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;
        User user = (User)request.getSession().getAttribute("user");
        if (user==null || user.getRole()!= Role.CLIENT) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            List<Account> accounts = new ArrayList<Account>();
            List<PaymentsList> allPayments = new ArrayList<PaymentsList>();
            List<TariffAddRequest> tariffAddRequests = new ArrayList<TariffAddRequest>();
            try {
                accounts = accountService.takeAccountsByClientId(user.getId());
                for (int i = 0; i < accounts.size(); i++) {
                    allPayments.add(paymentService.takePaymentsListByAccountId(accounts.get(i).getId()));
                }
                tariffAddRequests = accountService.takeAddTariffRequestsByClientId(user.getId());
            } catch (ServiceException e) {
                LOG.error("Error in ClientProfileCommand");
            }
            request.setAttribute(ATTR_ACCOUNTS, accounts);
            request.setAttribute(ATTR_PAYMENTS, allPayments);
            request.setAttribute(ATTR_REQUESTS, tariffAddRequests);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_PROFILE_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
