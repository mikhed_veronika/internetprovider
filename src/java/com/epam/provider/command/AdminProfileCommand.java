package com.epam.provider.command;

import com.epam.provider.entity.*;
import com.epam.provider.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command that redirects authorized user to admin profile.
 * This command can be used only by administrator.
 * @author Veronika Mikhed
 */

public class AdminProfileCommand implements Command{

    public static final Logger LOG = Logger.getLogger(AdminProfileCommand.class);

    public AdminProfileCommand() {}

    public String execute(HttpServletRequest request, HttpServletResponse response){
        String page;
        User user = (User)request.getSession().getAttribute("user");
        if (user==null || user.getRole()!= Role.ADMIN) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_PROFILE_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, REDIRECT_PAGE);
        return page;
    }
}
