package com.epam.provider.command;

import com.epam.provider.encryption.PasswordEncryption;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.mail.StarNetMailSender;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.ClientService;
import org.apache.log4j.Logger;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

/**
 * Command that registers new clients.
 * This command can be used by guests.
 * @author Veronika Mikhed
 */

public class RegisterCommand implements Command {

    public static final Logger LOG = Logger.getLogger(RegisterCommand.class);
    private ClientService clientService;

    private static final String PARAM_NAME_FIRST_NAME = "first-name";
    private static final String PARAM_NAME_LAST_NAME = "last-name";
    private static final String PARAM_NAME_PASSPORT = "passport";
    private static final String PARAM_NAME_ADDRESS = "address";
    private static final String PARAM_NAME_PHONE = "phone";
    private static final String PARAM_NAME_EMAIL = "email";
    private static final String PARAM_NAME_PASSWORD = "password";

    public static final String FIRST_NAME_REGEX = "[^\\s\\d]{2,}";
    public static final String LAST_NAME_REGEX = "[^\\s\\d]{2,}";
    public static final String PASSPORT_REGEX = "[A-Z][A-Z]([0-9]){7}";
    public static final String ADDRESS_REGEX = ".{1,}";
    public static final String PHONE_REGEX = "(\\+)?[0-9]{7,13}";
    public static final String EMAIL_REGEX = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}";
    public static final String PASSWORD_REGEX = ".{6,}";

    public static final String INCORRECT_DATA = "Incorrect data entered";

    public RegisterCommand() {
        clientService = new ClientService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        boolean res = false;
        String page;
        String firstName = request.getParameter(PARAM_NAME_FIRST_NAME);
        String lastName = request.getParameter(PARAM_NAME_LAST_NAME);
        String passport = request.getParameter(PARAM_NAME_PASSPORT);
        String address = request.getParameter(PARAM_NAME_ADDRESS);
        String phone = request.getParameter(PARAM_NAME_PHONE);
        String email = request.getParameter(PARAM_NAME_EMAIL);
        String password = request.getParameter(PARAM_NAME_PASSWORD);

        if (checkClientInfo(firstName, lastName, passport,address, phone, email, password)) {
            password = PasswordEncryption.md5(request.getParameter(PARAM_NAME_PASSWORD));
            try {
                res = clientService.addNewClient(firstName, lastName, passport, address, phone, email, password);
            } catch (ServiceException e) {
                LOG.error("ServiceException in RegisterCommand");
            }
            if (res) {
                StarNetMailSender mailSender = new StarNetMailSender();
                try {
                    mailSender.send(email, StarNetMailSender.REGISTER_SUCCESS_MESSAGE);
                } catch (MessagingException e) {
                    LOG.error("Sending message failed.");
                }
            }
        }
        page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        request.setAttribute(ATTR_RESULT, res ? SUCCESS_RESULT : INCORRECT_DATA);
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }

    private boolean checkClientInfo (String firstName, String lastName, String passport, String address,
                                           String phone, String email, String password) {
        return (Pattern.matches(FIRST_NAME_REGEX, firstName) && Pattern.matches(LAST_NAME_REGEX, lastName) &&
                Pattern.matches(PASSPORT_REGEX, passport) && Pattern.matches(ADDRESS_REGEX, address) &&
                Pattern.matches(PHONE_REGEX, phone) && Pattern.matches(EMAIL_REGEX, email) &&
                Pattern.matches(PASSWORD_REGEX, password));
    }

}