package com.epam.provider.command;


import com.epam.provider.entity.Role;
import com.epam.provider.entity.TariffAddRequest;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.AccountService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that redirects to handle requests for connection to tariffs.
 * This command can be used only by administrator.
 * @author Veronika Mikhed
 */

public class RedirectToHandleRequestsCommand implements Command{
    public static final Logger LOG = Logger.getLogger(RedirectToHandleRequestsCommand.class);

    public static final String ATTR_TARIFF_ADD_REQUESTS = "tariffAddRequests";

    private AccountService accountService;


    public RedirectToHandleRequestsCommand() {
        accountService = new AccountService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;
        List<TariffAddRequest> tariffAddRequests = new ArrayList<>();
        User user = (User)request.getSession().getAttribute("user");
        if (user!=null && user.getRole()== Role.ADMIN) {
            try {
                tariffAddRequests = accountService.takeAllTariffAddRequests();
            } catch (ServiceException e) {
                LOG.error("Error while tariffAddRequests loading");
            }
            request.setAttribute(ATTR_TARIFF_ADD_REQUESTS, tariffAddRequests);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_HANDLE_REQUESTS_PAGE_PATH);
        }
        else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }
}
