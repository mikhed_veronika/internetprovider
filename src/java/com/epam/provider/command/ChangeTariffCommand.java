package com.epam.provider.command;


import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.TariffService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

/**
 * Command that changes info about tariff plan.
 * This command can be used only by administrator.
 * @author Veronika Mikhed
 */

public class ChangeTariffCommand implements Command {

    public static final Logger LOG = Logger.getLogger(ChangeTariffCommand.class);

    private TariffService tariffService;

    public static final String PARAM_NAME_TARIFF_ID = "tariffId";
    public static final String PARAM_NAME_TARIFF_DESCRIPTION = "tariffDescription";
    public static final String PARAM_NAME_CURRENCY_CODE = "currencyCode";
    public static final String PARAM_NAME_MONTH_PAYMENT = "monthPayment";
    public static final String PARAM_NAME_UPLOAD_SPEED = "uploadSpeed";
    public static final String PARAM_NAME_DOWNLOAD_SPEED = "downloadSpeed";
    public static final String PARAM_NAME_TRAFFIC_VOLUME = "trafficVolume";

    public ChangeTariffCommand() {
        tariffService = new TariffService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        User user = (User)request.getSession().getAttribute("user");
        boolean res = false;
        int tariffId = Integer.valueOf(request.getParameter(PARAM_NAME_TARIFF_ID));
        String tariffDescription = request.getParameter(PARAM_NAME_TARIFF_DESCRIPTION);
        String currencyCode = request.getParameter(PARAM_NAME_CURRENCY_CODE);
        BigDecimal monthPayment = new BigDecimal(request.getParameter(PARAM_NAME_MONTH_PAYMENT));
        Double uploadSpeed = new Double(request.getParameter(PARAM_NAME_UPLOAD_SPEED));
        Double downloadSpeed = new Double(request.getParameter(PARAM_NAME_DOWNLOAD_SPEED));
        Double trafficVolume = new Double(request.getParameter(PARAM_NAME_TRAFFIC_VOLUME));

        if (user==null || user.getRole()!= Role.ADMIN) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            if (checkTariffInfo(monthPayment, uploadSpeed, downloadSpeed)) {
                try {
                    res = tariffService.changeTariff(tariffId, tariffDescription, currencyCode, monthPayment, uploadSpeed, downloadSpeed, trafficVolume);
                } catch (ServiceException e) {
                    LOG.error("Error in ChangeTariffCommand");
                }
            }
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_CHANGE_TARIFF_PAGE_PATH);
            request.setAttribute(ATTR_RESULT, res ? SUCCESS_RESULT : ERROR_RESULT);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;
    }

    public boolean checkTariffInfo (BigDecimal monthPayment, Double uploadSpeed, Double downloadSpeed) {
        boolean res = false;
        if (monthPayment.compareTo(new BigDecimal("0"))>0 && monthPayment.compareTo(new BigDecimal("1000000"))<0 &&
                uploadSpeed.compareTo(new Double("0"))>0 && uploadSpeed.compareTo(new Double("10000"))<=0 &&
                downloadSpeed.compareTo(new Double("0"))>0 && downloadSpeed.compareTo(new Double("10000"))<=0) {
            res = true;
        }
        return res;
    }


}
