package com.epam.provider.command;

import com.epam.provider.encryption.PasswordEncryption;
import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.manager.ConfigurationManager;
import com.epam.provider.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

/**
 * Command that changes password of user.
 * This command can be used both client and administrator.
 * @author Veronika Mikhed
 */

public class ChangePasswordCommand implements Command {

    public static final Logger LOG = Logger.getLogger(ChangePasswordCommand.class);
    private UserService userService;

    public static final String PARAM_NAME_CURRENT_PASS = "currentPassword";
    public static final String PARAM_NAME_NEW_PASS = "newPassword";
    public static final String ATTR_INCORRECT_PASSWORD = "Incorrect password";

    public static final String PASSWORD_REGEX = ".{6,}";

    public ChangePasswordCommand() {
        userService = new UserService();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response){
        String page = null;
        User user = (User)request.getSession().getAttribute("user");
        boolean res = false;
        String currPassword = request.getParameter(PARAM_NAME_CURRENT_PASS);
        String newPassword = request.getParameter(PARAM_NAME_NEW_PASS);

        if (user==null) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
        }
        else {
            currPassword = PasswordEncryption.md5(currPassword);
            newPassword = PasswordEncryption.md5(newPassword);
            try {
                if (userService.checkPassword(currPassword, user.getEmail()) && Pattern.matches(PASSWORD_REGEX, newPassword)) {
                    res = userService.changePassword(user.getId(), newPassword, user.getRole());
                }
            } catch (ServiceException e) {
                LOG.error("Error in ChangePasswordCommand");
            }
            page = (user.getRole()==Role.ADMIN) ? (ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_CHANGE_PASSWORD_PAGE_PATH)) :
                    (ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_CHANGE_PASSWORD_PAGE_PATH));

            request.setAttribute(ATTR_RESULT, res ? SUCCESS_RESULT : ATTR_INCORRECT_PASSWORD);
        }
        request.setAttribute(ATTR_PAGE, FORWARD_PAGE);
        return page;

    }

}
