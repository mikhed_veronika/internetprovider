package com.epam.provider.test.service;

import com.epam.provider.exception.ServiceException;
import com.epam.provider.service.TariffService;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.Test;

public class TariffServiceTest {

    private static Logger LOG = Logger.getLogger(TariffServiceTest.class);
    private TariffService tariffService = new TariffService();
    @Test
    public void takeActiveTariffsTest() {
        try {
            Assert.assertNotNull(tariffService.takeAllActiveTariffsList());
        } catch (ServiceException e) {
            LOG.info("Test taking tariffs failed (takeActiveTariffsTest)");
        }
    }
}
