package com.epam.provider.test.service;

import com.epam.provider.entity.User;
import com.epam.provider.exception.ServiceException;
import com.epam.provider.service.UserService;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.Test;

public class UserServiceTest {

    private static Logger LOG = Logger.getLogger(UserServiceTest.class);
    private UserService userService = new UserService();
    @Test
    public void failAuthenticationTest() {
        String email = "qwerty";
        String password = "123456";
        try {
            Assert.assertNull(userService.authenticate(email, password));
        } catch (ServiceException e) {
            LOG.info("Test authentication failed (failAuthenticationTest)");
        }
    }

    @Test
    public void successAuthenticationTest() {
        String email = "petrov.petr@gmail.com";
        String password = "f396c3b74762b1fee69b10abb875139b";
        User user;
        try {
            user = userService.authenticate(email, password);
            Assert.assertNotNull(user);
            Assert.assertEquals("TK6239852", user.getPassport());
        } catch (ServiceException e) {
            LOG.info("Test authentication failed (successAuthenticationTest");
        }
    }
}
