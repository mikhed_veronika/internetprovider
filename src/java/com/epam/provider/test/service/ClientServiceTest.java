package com.epam.provider.test.service;

import com.epam.provider.exception.ServiceException;
import com.epam.provider.service.ClientService;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.Test;

public class ClientServiceTest {

    private static Logger LOG = Logger.getLogger(ClientServiceTest.class);
    private ClientService clientService = new ClientService();

    @Test
    public void failRegistrationTest() {
        boolean res;
        try {
            res = clientService.addNewClient("Firstname", "Lastname", "AB1234567", "address",
                    "+375298521478", "mikhed.veronika@gmail.com", "password");
            Assert.assertEquals(false, res);
        } catch (ServiceException e) {
            LOG.info("Error in failRegistrationTest");
        }
    }
}
