package com.epam.provider.test.resource;

import com.epam.provider.manager.ConfigurationManager;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.Assume;
import org.junit.Test;

public class ResourceTest {

    private static Logger LOG = Logger.getLogger(ResourceTest.class);

    @Test
    public void initResourceTest() {
        try {
            Assert.assertNotNull(ConfigurationManager.getInstance());
        } catch (Exception e) {
            LOG.info("Can not init resource");
        }
    }

    @Test
    public void getPropertyTest() {
        Assert.assertEquals("/index.jsp", ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_PAGE_PATH));
        Assert.assertEquals("/jsp/login.jsp", ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH));
        Assert.assertEquals("/jsp/tariffs.jsp", ConfigurationManager.getInstance().getProperty(ConfigurationManager.TARIFFS_PAGE_PATH));
        Assert.assertEquals("/jsp/comments.jsp", ConfigurationManager.getInstance().getProperty(ConfigurationManager.COMMENTS_PAGE_PATH));
    }
}
