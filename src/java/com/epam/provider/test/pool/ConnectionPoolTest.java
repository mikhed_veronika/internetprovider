package com.epam.provider.test.pool;

import com.epam.provider.connect.ConnectionPool;
import com.epam.provider.connect.ProxyConnection;
import com.epam.provider.exception.ConnectionPoolException;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

public class ConnectionPoolTest {

    private static Logger LOG = Logger.getLogger(ConnectionPoolTest.class);

    @BeforeClass
    public static void initPoolTest() {
        try {
            ConnectionPool pool = ConnectionPool.getInstance();
            Assert.assertNotNull(pool);
            Assert.assertNotSame(0, ConnectionPool.getInstance().getPoolSize());
        } catch (Exception e) {
            LOG.info("Error in ConnectionPool.getInstance() (initPoolTest)");
        }
    }

    @Test
    public void poolSizeTest() {
        int expectedSize = 32;
        Assert.assertEquals(expectedSize, ConnectionPool.getInstance().getPoolSize());
    }

    @Test
    public void getConnectionTest() {
        ProxyConnection proxyConnection = null;
        try {
            proxyConnection = ConnectionPool.getInstance().getConnection();
            Assert.assertNotNull(proxyConnection);
        } catch (ConnectionPoolException e) {
            LOG.info("Can not get connection (getConnectionTest)");
        }
    }

    @Test
    public void returnConnectionToPoolTest() {
        try {
            int connectionsCount = ConnectionPool.getInstance().getConnectionCount();
            ProxyConnection proxyConnection = ConnectionPool.getInstance().getConnection();
            Assert.assertEquals(connectionsCount-1, ConnectionPool.getInstance().getConnectionCount());
            ConnectionPool.getInstance().returnConnection(proxyConnection);
            Assert.assertEquals(connectionsCount, ConnectionPool.getInstance().getConnectionCount());
        } catch (Exception e) {
            LOG.info("Can not return connection (returnConnectionToPoolTest)");
        }
    }
}
