package com.epam.provider.dao;

import com.epam.provider.connect.ConnectionPool;
import com.epam.provider.connect.ProxyConnection;
import com.epam.provider.entity.Comment;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ConnectionPoolException;
import com.epam.provider.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains sql queries.
 * Provides methods that get data and insert data in table 'comments' in DB
 * @author Veronika Mikhed
 * @see Comment
 */

public class CommentDAO extends AbstractDAO<Integer, Comment> {

    private static Logger LOG = Logger.getLogger(CommentDAO.class);

    public static final String SQL_SELECT_ALL_COMMENTS = "SELECT " +
            "comments.comment_id, comments.client_id, comments.header, comments.text, comments.time," +
            "clients.first_name, clients.last_name FROM comments JOIN clients ON comments.client_id = clients.client_id";

    public static final String SQL_SELECT_COMMENT_BY_ID = "SELECT " +
            "comments.comment_id, comments.client_id, comments.header, comments.text, comments.time," +
            "clients.first_name, clients.last_name FROM comments JOIN clients ON comments.client_id = clients.client_id " +
            "WHERE comment_id=?";

    public static final String SQL_INSERT_COMMENT = "INSERT INTO comments " +
            "(client_id, header, text, time) VALUES(?,?,?, NOW())";

    public static final String SQL_DELETE_COMMENT = "DELETE FROM comments WHERE comment_id=?";

    /**
     * Method that takes all comments from table 'comments'
     * @return list of comments
     * @throws DAOException
     */

    @Override
    public List<Comment> findAll() throws DAOException {
        List<Comment> comments = new ArrayList<>();
        ProxyConnection cn = null;
        Statement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.createStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_COMMENTS);
            while (resultSet.next()) {
                Comment comment = initComment(resultSet);
                comments.add(comment);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception (request or table failed): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return comments;
    }

    /**
     * Method that takes comment from table 'comments' by unique id
     * @param key is comment id
     * @return entity Comment
     * @throws DAOException
     */

    @Override
    public Comment findEntityByKey(Integer key) throws DAOException {
        Comment comment = new Comment();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_COMMENT_BY_ID);
            st.setInt(1, key);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            comment = initComment(resultSet);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception (request or table failed): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return comment;
    }

    /**
     * Method that inserts a new comment to table 'comments'
     * @param client_id
     * @param header
     * @param text
     * @return boolean result
     * @throws DAOException
     */

    public boolean insertComment(int client_id, String header, String text) throws DAOException {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(SQL_INSERT_COMMENT);
            stm.setInt(1, client_id);
            stm.setString(2, header);
            stm.setString(3, text);
            stm.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method insertComment" + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that deletes from table 'comments' comment that id equals commentId
     * @param commentId
     * @return boolean result
     * @throws DAOException
     */

    public boolean deleteComment(int commentId) throws DAOException {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(SQL_DELETE_COMMENT);
            stm.setInt(1, commentId);
            stm.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method deleteComment" + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that creates entity object Comment based on data contained in resultSet
     * @param resultSet
     * @return object Comment
     * @throws SQLException
     */

    private Comment initComment (ResultSet resultSet) throws SQLException{
        Comment comment = new Comment();
        comment.setId(resultSet.getInt("comment_id"));
        comment.setClientId(resultSet.getInt("client_id"));
        comment.setFirstName(resultSet.getString("clients.first_name"));
        comment.setLastName(resultSet.getString("clients.last_name"));
        comment.setHeader(resultSet.getString("header"));
        comment.setText(resultSet.getString("text"));
        comment.setDateTime(resultSet.getDate("time"));
        return comment;
    }
}