package com.epam.provider.dao;


import com.epam.provider.connect.ConnectionPool;
import com.epam.provider.connect.ProxyConnection;
import com.epam.provider.exception.ConnectionPoolException;
import com.epam.provider.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Abstract class that describes structure of classes that included in DAO layer
 * @author Veronika Mikhed
 * @param <K> key
 * @param <T> type of entity
 */

public abstract class AbstractDAO <K, T> {

    private static Logger LOG = Logger.getLogger(AbstractDAO.class);

    /**
     * Method that takes all entities found in table of database
     * @return list
     * @throws DAOException
     */

    public abstract List<T> findAll() throws DAOException;

    /**
     * Method that finds entity by unique field
     * @param key is any unique field
     * @return entity founded
     * @throws DAOException
     */

    public abstract T findEntityByKey(K key) throws DAOException;

    public void closeStatement (Statement st) {
        if (st!=null) {
            try {
                st.close();
            } catch (SQLException e) {
                LOG.error ("Can not close statement");
            }
        }
    }

    /**
     * Method for closing prepared statement
     * @param pst
     */

    public void closeStatement (PreparedStatement pst) {
        if (pst!=null) {
            try {
                pst.close();
            } catch (SQLException e) {
                LOG.error ("Can not close statement");
            }
        }
    }

    /**
     * Method for checking unique Entity by key
     * @param key is unique property
     * @param query is sql-query
     * @return true if unique
     */

    public boolean checkUnique (String key, String query) {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(query);
            stm.setString(1, key);
            ResultSet resultSet = stm.executeQuery();
            resultSet.next();
            if (resultSet.getInt("count(*)") == 0) {
                res = true;
            }
        } catch (ConnectionPoolException | SQLException e) {
            LOG.error("Exception in method 'checkUniqueName()'");
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }
}
