package com.epam.provider.dao;

import com.epam.provider.connect.ConnectionPool;
import com.epam.provider.connect.ProxyConnection;
import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ConnectionPoolException;
import com.epam.provider.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains sql queries.
 * Provides methods that get data and insert data in table 'clients' in DB
 * @author Veronika Mikhed
 * @see User
 */

public class ClientDAO extends AbstractDAO<String, User> {

    private static Logger LOG = Logger.getLogger(ClientDAO.class);

    public static final String SQL_SELECT_ALL_CLIENTS = "SELECT " +
            "client_id, first_name, last_name, passport, address, phone_number, email, password FROM clients";

    public static final String SQL_SELECT_CLIENT_BY_EMAIL = "SELECT " +
            "client_id, first_name, last_name, passport, address, phone_number, email, password FROM clients WHERE email=?";

    public static final String SQL_INSERT_CLIENT = "INSERT INTO clients " +
            "(first_name, last_name, passport, address, phone_number, email, password) VALUES(?,?,?,?,?,?,?)";

    public static final String SQL_SELECT_EMAIL_BY_CLIENT_ID = "SELECT " +
            "email FROM clients WHERE client_id=?";

    public static final String SQL_SELECT_USER_COUNT_BY_EMAIL = "SELECT count(*) FROM (SELECT email FROM clients UNION " +
            "SELECT email FROM administrators) AS u WHERE u.email=?";

    /**
     * Method that takes all clients from table 'clients'
     * @return list of users (clients)
     * @throws DAOException
     */

    @Override
    public List<User> findAll() throws DAOException {
        List<User> clients = new ArrayList<>();
        ProxyConnection cn = null;
        Statement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.createStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_CLIENTS);
            while (resultSet.next()) {
                User client = initClient(resultSet);
                clients.add(client);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method findAll(): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return clients;
    }

    /**
     * Method that takes client that email equals key
     * @param key is email
     * @return entity User
     * @throws DAOException
     */

    @Override
    public User findEntityByKey(String key) throws DAOException {
        User client = new User();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_CLIENT_BY_EMAIL);
            st.setString(1, key);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            client = initClient(resultSet);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method findEntityByKey(): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return client;
    }

    /**
     * Method that inserts new client in table 'clients'.
     * At first it checks if the email of new client is unique.
     * @param firstName
     * @param lastName
     * @param passport
     * @param address
     * @param phone
     * @param email
     * @param password
     * @return boolean result
     * @throws DAOException
     */

    public boolean insertClient(String firstName, String lastName, String passport, String address,
                                String phone, String email, String password) throws DAOException{
        boolean res = false;
        if (!checkUnique(email, SQL_SELECT_USER_COUNT_BY_EMAIL)) {
            return res;
        }
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(SQL_INSERT_CLIENT);
            stm.setString(1, firstName);
            stm.setString(2, lastName);
            stm.setString(3, passport);
            stm.setString(4, address);
            stm.setString(5, phone);
            stm.setString(6, email);
            stm.setString(7, password);
            stm.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method insertClient" + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that takes email of client that id equals clientId
     * @param clientId
     * @return email of client
     * @throws DAOException
     */

    public String findEmailByClientId(int clientId) throws DAOException {
        String email;
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_EMAIL_BY_CLIENT_ID);
            st.setInt(1, clientId);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            email = resultSet.getString("email");
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method findEmailByClientId(): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return email;
    }

    /**
     * Method that creates entity object 'User' with role 'CLIENT' based data contained in resultSet
     * @param resultSet
     * @return object User
     * @throws SQLException
     */

    private User initClient (ResultSet resultSet) throws SQLException{
        User client = new User();
        client.setId(resultSet.getInt("client_id"));
        client.setFirstName(resultSet.getString("first_name"));
        client.setLastName(resultSet.getString("last_name"));
        client.setPassport(resultSet.getString("passport"));
        client.setAddress(resultSet.getString("address"));
        client.setPhoneNumber(resultSet.getString("phone_number"));
        client.setEmail(resultSet.getString("email"));
        client.setPassword(resultSet.getString("password"));
        client.setRole(Role.CLIENT);
        return client;
    }
}
