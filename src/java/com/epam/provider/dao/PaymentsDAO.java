package com.epam.provider.dao;

import com.epam.provider.connect.ConnectionPool;
import com.epam.provider.connect.ProxyConnection;
import com.epam.provider.entity.PaymentInfo;
import com.epam.provider.entity.PaymentsList;
import com.epam.provider.exception.ConnectionPoolException;
import com.epam.provider.exception.DAOException;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains sql queries.
 * Provides methods that get data and insert data in table 'payments' in DB
 * @author Veronika Mikhed
 * @see PaymentInfo
 * @see PaymentsList
 */

public class PaymentsDAO extends AbstractDAO<Long, PaymentInfo>{

    private static Logger LOG = Logger.getLogger(PaymentsDAO.class);

    public static final String SQL_SELECT_ALL_PAYMENTS = "SELECT payment_id, account_id, payment_date, payment_sum FROM payments";

    public static final String SQL_SELECT_PAYMENTS_BY_ACCOUNT_ID = "SELECT " +
            "payment_id, account_id, payment_date, payment_sum FROM payments WHERE account_id=?";

    public static final String SQL_SELECT_PAYMENT_BY_ID = "SELECT " +
            "payment_id, account_id, payment_date, payment_sum FROM payments WHERE payment_id=?";

    public static final String SQL_INSERT_PAYMENT = "INSERT INTO payments " +
            "(account_id, payment_date, payment_sum) VALUES(?,NOW(),?)";

    public static final String SQL_UPDATE_ADD_BALANCE_BY_ACCOUNT_ID = "UPDATE " +
            "accounts SET balance=balance+? WHERE account_id=?";


    /**
     * Method that takes all paymentInfo from table 'payments'
     * @return list of paymentInfo
     * @throws DAOException
     */

    @Override
    public List<PaymentInfo> findAll() throws DAOException {
        List<PaymentInfo> payments = new ArrayList<>();
        ProxyConnection cn = null;
        Statement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.createStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_PAYMENTS);
            while (resultSet.next()) {
                PaymentInfo payment = initPayment(resultSet);
                payments.add(payment);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception (request or table failed): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return payments;
    }

    /**
     * Method that takes paymentInfo from table 'payments' by unique id
     * @param key is payment id
     * @return entity PaymentInfo
     * @throws DAOException
     */

    @Override
    public PaymentInfo findEntityByKey(Long key) throws DAOException {
        PaymentInfo payment = new PaymentInfo();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_PAYMENT_BY_ID);
            st.setLong(1, key);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            payment = initPayment(resultSet);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception (request or table failed): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return payment;
    }

    /**
     * Method that takes payments list by account that id equals accountId
     * @param accountId
     * @return payments list
     * @throws DAOException
     */

    public PaymentsList takePaymentsListByAccountId(int accountId) throws DAOException {
        PaymentsList list = new PaymentsList();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_PAYMENTS_BY_ACCOUNT_ID);
            st.setInt(1, accountId);
            ResultSet resultSet = st.executeQuery();
            list.setAccountId(accountId);
            while(resultSet.next()) {
                list.addPaymentInfo(initPayment(resultSet));
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception (request or table failed): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return list;
    }

    /**
     * Method that increase balance of account that id equals accountId
     * @param accountId
     * @param paymentSum
     * @param cardNumber
     * @param cvv2
     * @return boolean result
     * @throws DAOException
     */

    public boolean addMoneyToAccount(int accountId, BigDecimal paymentSum, String cardNumber, String cvv2) throws DAOException {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement stmPay = null;
        PreparedStatement stmInsertPayment = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            cn.setAutoCommit(false);
            stmPay = cn.prepareStatement(SQL_UPDATE_ADD_BALANCE_BY_ACCOUNT_ID);
            stmPay.setBigDecimal(1, paymentSum);
            stmPay.setInt(2, accountId);
            stmPay.executeUpdate();
            stmInsertPayment = cn.prepareStatement(SQL_INSERT_PAYMENT);
            stmInsertPayment.setInt(1, accountId);
            stmInsertPayment.setBigDecimal(2, paymentSum);
            stmInsertPayment.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method addMoneyToAccount()" + e);
        } finally {
            try {
                if (cn != null) {
                    if (res) {
                        cn.commit();
                    } else {
                        cn.rollback();
                    }
                    cn.setAutoCommit(true);
                }
            } catch (SQLException e) {
                LOG.error("setAutoCommit(true) is failed in method addMoneyToAccount");
            }
            closeStatement(stmPay);
            closeStatement(stmInsertPayment);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }


    /**
     * Method that creates object PaymentInfo based on data contains in resultSet
     * @param resultSet
     * @return object PaymentInfo
     * @throws SQLException
     */

    private PaymentInfo initPayment (ResultSet resultSet) throws SQLException{
        PaymentInfo payment = new PaymentInfo();
        payment.setPaymentId(resultSet.getLong("payment_id"));
        payment.setPaymentDate(resultSet.getDate("payment_date"));
        payment.setPaymentSum(resultSet.getBigDecimal("payment_sum"));
        return payment;
    }
}
