package com.epam.provider.dao;

import com.epam.provider.connect.ConnectionPool;
import com.epam.provider.connect.ProxyConnection;
import com.epam.provider.entity.Tariff;
import com.epam.provider.exception.ConnectionPoolException;
import com.epam.provider.exception.DAOException;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains sql queries.
 * Provides methods that get data and insert data in table 'tariffs' in DB
 * @author Veronika Mikhed
 * @see Tariff
 */

public class TariffDAO extends AbstractDAO<String, Tariff> {

    private static Logger LOG = Logger.getLogger(TariffDAO.class);

    public static final String SQL_SELECT_ALL_TARIFFS = "SELECT " +
            "tariff_id, tariff_name, description, currency_code, month_payment, upload_speed, download_speed, " +
            "start_date, finish_date, traffic_volume FROM tariffs";

    public static final String SQL_SELECT_ALL_ACTIVE_TARIFFS = "SELECT " +
            "tariff_id, tariff_name, description, currency_code, month_payment, upload_speed, download_speed, " +
            "start_date, finish_date, traffic_volume FROM tariffs WHERE finish_date IS NULL";

    public static final String SQL_SELECT_ALL_CLOSED_TARIFFS = "SELECT " +
            "tariff_id, tariff_name, description, currency_code, month_payment, upload_speed, download_speed, " +
            "start_date, finish_date, traffic_volume FROM tariffs WHERE finish_date IS NOT NULL";

    public static final String SQL_SELECT_TARIFF_BY_NAME = "SELECT " +
            "tariff_id, tariff_name, description, currency_code, month_payment, upload_speed, download_speed, " +
            "start_date, finish_date, traffic_volume FROM tariffs WHERE tariff_name=?";

    public static final String SQL_SELECT_TARIFF_BY_ID = "SELECT " +
            "tariff_id, tariff_name, description, currency_code, month_payment, upload_speed, download_speed, " +
            "start_date, finish_date, traffic_volume FROM tariffs WHERE tariff_id=?";

    public static final String SQL_INSERT_TARIFF = "INSERT INTO tariffs " +
            "(tariff_name, description, currency_code, month_payment, upload_speed, download_speed, " +
            "start_date, traffic_volume) " +
            "VALUES(?,?,?,?,?,?,NOW(),?)";

    public static final String SQL_UPDATE_TARIFF_BY_ID = "UPDATE tariffs SET " +
            "description=?, currency_code=?, month_payment=?, upload_speed=?, download_speed=?, traffic_volume=? " +
            "WHERE tariff_id=?";

    public static final String SQL_SELECT_TARIFF_COUNT_BY_NAME = "SELECT count(*) FROM tariffs WHERE tariff_name=?";

    public static final String SQL_CLOSE_TARIFF = "UPDATE tariffs SET finish_date=NOW() WHERE tariff_id=?";

    /**
     * Method that takes all tariffs from table 'tariffs'
     * @return list of all tariffs
     * @throws DAOException
     */

    @Override
    public List<Tariff> findAll() throws DAOException {
        return takeTariffsByQuery(SQL_SELECT_ALL_TARIFFS);
    }

    /**
     * Method that takes tariff from table 'tariffs' by unique id
     * @param key is tariff name
     * @return entity Tariff
     * @throws DAOException
     */

    @Override
    public Tariff findEntityByKey(String key) throws DAOException {
        Tariff tariff = new Tariff();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_TARIFF_BY_NAME);
            st.setString(1, key);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            tariff = initTariff(resultSet);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method findEntityByKey(String key): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return tariff;
    }

    /**
     * Method that takes tariff from table 'tariffs' that id equals tariffId
     * @param tariffId
     * @return entity Tariff
     * @throws DAOException
     */

    public Tariff takeTariffById(int tariffId) throws DAOException {
        Tariff tariff = new Tariff();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_TARIFF_BY_ID);
            st.setInt(1, tariffId);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            tariff = initTariff(resultSet);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method takeTariffById(int tariffId): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return tariff;
    }

    /**
     * Method that takes all active tariffs (tariff is active if field 'finish_date' of it's tariff is null)
     * @return entity Tariff
     * @throws DAOException
     */

    public List<Tariff> findAllActiveTariffs() throws DAOException {
        return takeTariffsByQuery(SQL_SELECT_ALL_ACTIVE_TARIFFS);
    }

    /**
     * Method that takes all closed tariffs (tariff is closed if field 'finish_date' of it's tariff is not null)
     * @return entity Tariff
     * @throws DAOException
     */

    public List<Tariff> findAllClosedTariffs() throws DAOException {
        return takeTariffsByQuery(SQL_SELECT_ALL_CLOSED_TARIFFS);
    }

    /**
     * Method that takes all tariff by sql query
     * @param query
     * @return list of tariffs
     * @throws DAOException
     */

    private List<Tariff> takeTariffsByQuery (String query) throws DAOException{
        List<Tariff> tariffs = new ArrayList<>();
        ProxyConnection cn = null;
        Statement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.createStatement();
            ResultSet resultSet = st.executeQuery(query);
            while (resultSet.next()) {
                Tariff tariff = initTariff(resultSet);
                tariffs.add(tariff);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception (request or table failed): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return tariffs;
    }

    /**
     * Method that inserts a new tariff in table 'tariffs'
     * @param tariffName
     * @param description
     * @param currencyCode
     * @param monthPayment
     * @param uploadSpeed
     * @param downloadSpeed
     * @param trafficVolume
     * @return boolean result
     * @throws DAOException
     */

    public boolean insertTariff(String tariffName, String description, String currencyCode, BigDecimal monthPayment, Double uploadSpeed,
                                Double downloadSpeed, Double trafficVolume) throws DAOException {
        boolean res = false;
        if (!checkUnique(tariffName, SQL_SELECT_TARIFF_COUNT_BY_NAME)) {
            return res;
        }
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(SQL_INSERT_TARIFF);
            stm.setString(1, tariffName);
            stm.setString(2, description);
            stm.setString(3, currencyCode);
            stm.setBigDecimal(4, monthPayment);
            stm.setDouble(5, uploadSpeed);
            stm.setDouble(6, downloadSpeed);
            stm.setDouble(7, trafficVolume);
            stm.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method insertTariff" + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that updates values of tariff that id equals tariffId
     * @param tariffId
     * @param description
     * @param currencyCode
     * @param monthPayment
     * @param uploadSpeed
     * @param downloadSpeed
     * @param trafficVolume
     * @return boolean result
     * @throws DAOException
     */

    public boolean changeTariff(int tariffId, String description, String currencyCode, BigDecimal monthPayment, Double uploadSpeed,
                                Double downloadSpeed, Double trafficVolume) throws DAOException {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(SQL_UPDATE_TARIFF_BY_ID);
            stm.setString(1, description);
            stm.setString(2, currencyCode);
            stm.setBigDecimal(3, monthPayment);
            stm.setDouble(4, uploadSpeed);
            stm.setDouble(5, downloadSpeed);
            stm.setDouble(6, trafficVolume);
            stm.setInt(7, tariffId);
            stm.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method changeTariff" + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that closes tariff that id equals tariffId.
     * Closing tariff means setting date at field 'finish_date'.
     * @param tariffId
     * @return boolean result
     * @throws DAOException
     */

    public boolean closeTariff(int tariffId) throws DAOException {
        boolean res=false;
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(SQL_CLOSE_TARIFF);
            stm.setInt(1, tariffId);
            stm.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method closeTariff(int tariffId)" + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that creates object Tariff based on data contained in resultSet
     * @param resultSet
     * @return object Tariff
     * @throws SQLException
     */

    private Tariff initTariff (ResultSet resultSet) throws SQLException{
        Tariff tariff = new Tariff();
        tariff.setId(resultSet.getInt("tariff_id"));
        tariff.setTariffName(resultSet.getString("tariff_name"));
        tariff.setDescription(resultSet.getString("description"));
        tariff.setCurrencyCode(resultSet.getString("currency_code"));
        tariff.setMonthPayment(resultSet.getBigDecimal("month_payment"));
        tariff.setUploadSpeed(resultSet.getDouble("upload_speed"));
        tariff.setDownloadSpeed(resultSet.getDouble("download_speed"));
        tariff.setStartDate(resultSet.getDate("start_date"));
        tariff.setFinishDate(resultSet.getDate("finish_date"));
        tariff.setTrafficVolume(resultSet.getDouble("traffic_volume"));
        return tariff;
    }
}