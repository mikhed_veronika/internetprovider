package com.epam.provider.dao;

import com.epam.provider.connect.ConnectionPool;
import com.epam.provider.connect.ProxyConnection;
import com.epam.provider.entity.Account;

import com.epam.provider.entity.TariffAddRequest;
import com.epam.provider.exception.ConnectionPoolException;
import com.epam.provider.exception.DAOException;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains sql queries.
 * Provides methods that get data and insert data in tables 'accounts' and 'tariff_add_requests' in DB
 * @author Veronika Mikhed
 * @see Account
 * @see TariffAddRequest
 */

public class AccountDAO extends AbstractDAO<Integer, Account> {

    private static Logger LOG = Logger.getLogger(AccountDAO.class);

    public static final String SQL_SELECT_ALL_ACCOUNTS = "SELECT " +
            "accounts.account_id, accounts.client_id, accounts.creating_date, accounts.end_date, accounts.balance, " +
            "accounts.tariff_id, accounts.address, accounts.ban, tariffs.tariff_name, tariffs.month_payment " +
            "FROM accounts JOIN tariffs ON accounts.tariff_id = tariffs.tariff_id";

    public static final String SQL_SELECT_ACCOUNT_BY_ID = "SELECT " +
            "accounts.account_id, accounts.client_id, accounts.creating_date, accounts.end_date, accounts.balance, " +
            "accounts.tariff_id, accounts.address, accounts.ban, tariffs.tariff_name, tariffs.month_payment " +
            "FROM accounts JOIN tariffs ON accounts.tariff_id = tariffs.tariff_id WHERE accounts.account_id=?";

    public static final String SQL_SELECT_ACCOUNTS_BY_CLIENT_ID = "SELECT " +
            "accounts.client_id, accounts.account_id, accounts.creating_date, accounts.end_date, accounts.balance, " +
            "accounts.tariff_id, accounts.address, accounts.ban, tariffs.tariff_name, tariffs.month_payment " +
            "FROM accounts JOIN tariffs ON accounts.tariff_id = tariffs.tariff_id WHERE accounts.client_id=?";

    public static final String SQL_SELECT_ACTIVE_ACCOUNTS_BY_CLIENT_ID = "SELECT " +
            "accounts.client_id, accounts.account_id, accounts.creating_date, accounts.end_date, accounts.balance, " +
            "accounts.tariff_id, accounts.address, accounts.ban, tariffs.tariff_name, tariffs.month_payment " +
            "FROM accounts JOIN tariffs ON accounts.tariff_id = tariffs.tariff_id WHERE accounts.client_id=? AND accounts.end_date IS NULL";

    public static final String SQL_SELECT_ACTIVE_ACCOUNTS_TO_BAN = "SELECT accounts.client_id, accounts.account_id, accounts.creating_date, " +
            "accounts.end_date, accounts.balance, accounts.tariff_id, accounts.address, accounts.ban, tariffs.tariff_name, " +
            "tariffs.month_payment FROM accounts JOIN tariffs ON accounts.tariff_id = tariffs.tariff_id " +
            "WHERE (accounts.ban='0' AND accounts.end_date IS NULL AND accounts.balance < 0)";

    public static final String SQL_SELECT_ACTIVE_ACCOUNTS_TO_UNBAN = "SELECT accounts.client_id, accounts.account_id, accounts.creating_date, " +
            "accounts.end_date, accounts.balance, accounts.tariff_id, accounts.address, accounts.ban, tariffs.tariff_name, " +
            "tariffs.month_payment FROM accounts JOIN tariffs ON accounts.tariff_id = tariffs.tariff_id " +
            "WHERE (accounts.ban='1' AND accounts.end_date IS NULL AND accounts.balance >= 0)";

    public static final String SQL_UPDATE_BAN_BY_ACCOUNT_ID = "UPDATE " +
            "accounts SET ban=? WHERE account_id=?";

    public static final String SQL_INSERT_ACCOUNT = "INSERT INTO accounts " +
            "(client_id, creating_date, balance, tariff_id, address) " +
            "VALUES(?,NOW(), 0.0 - (SELECT tariffs.month_payment FROM tariffs WHERE tariff_id=?),?,?)";

    public static final String SQL_CLOSE_ACCOUNT = "UPDATE accounts SET end_date=NOW() WHERE account_id=?";

    public static final String SQL_SELECT_ALL_TARIFF_ADD_REQUESTS = "SELECT " +
            "tariff_add_requests.request_id, tariff_add_requests.client_id, tariff_add_requests.tariff_id, tariff_add_requests.address, " +
            "tariffs.tariff_name " +
            "FROM tariff_add_requests JOIN tariffs ON tariff_add_requests.tariff_id = tariffs.tariff_id";

    public static final String SQL_SELECT_TARIFF_ADD_REQUEST_BY_ID = "SELECT " +
            "tariff_add_requests.request_id, tariff_add_requests.client_id, tariff_add_requests.tariff_id, tariff_add_requests.address, " +
            "tariffs.tariff_name " +
            "FROM tariff_add_requests JOIN tariffs ON tariff_add_requests.tariff_id = tariffs.tariff_id WHERE request_id=?";

    public static final String SQL_SELECT_TARIFF_ADD_REQUEST_BY_CLIENT = "SELECT " +
            "tariff_add_requests.request_id, tariff_add_requests.client_id, tariff_add_requests.tariff_id, tariff_add_requests.address, " +
            "tariffs.tariff_name " +
            "FROM tariff_add_requests JOIN tariffs ON tariff_add_requests.tariff_id = tariffs.tariff_id " +
            "JOIN clients ON tariff_add_requests.client_id = clients.client_id " +
            "WHERE clients.client_id=?";

    public static final String SQL_INSERT_TARIFF_ADD_REQUEST = "INSERT INTO tariff_add_requests " +
            "(client_id, tariff_id, address) VALUES(?,?,?)";

    public static final String SQL_DELETE_TARIFF_ADD_REQUEST_BY_ID = "DELETE FROM tariff_add_requests WHERE request_id=?";

    public static final String SQL_UPDATE_SUB_BALANCE_BY_ACCOUNT_ID = "UPDATE " +
            "accounts SET balance=balance-? WHERE account_id=?";

    public static final String SQL_SELECT_ABONENT_PAYMENT_BY_ACCOUNT_ID = "SELECT " +
            "tariffs.month_payment FROM accounts JOIN tariffs ON accounts.tariff_id = tariffs.tariffs_id " +
            "WHERE accounts.account_id=?";

    /**
     * Method that takes all accounts from table 'accounts'
     * @return list of accounts
     * @throws DAOException
     */

    @Override
    public List<Account> findAll() throws DAOException {
        return takeAccountsByQuery(SQL_SELECT_ALL_ACCOUNTS);
    }

    /**
     * Method that takes account by integer key (accountId)
     * @param key is account id
     * @return account entity
     * @throws DAOException
     */

    @Override
    public Account findEntityByKey(Integer key) throws DAOException {
        Account account = new Account();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_ACCOUNT_BY_ID);
            st.setInt(1, key);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            account = initAccount(resultSet);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method findEntityByKey(Integer key): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return account;
    }

    /**
     * Method that takes accounts which can be banned
     * @return list of accounts
     * @throws DAOException
     */

    public List<Account> findAccountsToBan() throws DAOException {
        return takeAccountsByQuery(SQL_SELECT_ACTIVE_ACCOUNTS_TO_BAN);
    }

    /**
     * Method that takes accounts which can be unbanned
     * @return list of accounts
     * @throws DAOException
     */

    public List<Account> findAccountsToUnban() throws DAOException {
        return takeAccountsByQuery(SQL_SELECT_ACTIVE_ACCOUNTS_TO_UNBAN);
    }

    /**
     * Method that set ban to account that id equals accountId
     * @param accountId
     * @param ban
     * @return boolean result
     * @throws DAOException
     */

    public boolean setBan (int accountId, boolean ban) throws DAOException {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_UPDATE_BAN_BY_ACCOUNT_ID);
            st.setString(1, ban ? "1" : "0");
            st.setInt(2, accountId);
            st.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method setBan()" + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that takes all accounts of client that id equals clientId
     * @param clientId
     * @return list of accounts
     * @throws DAOException
     */

    public List<Account> takeAccountsByClientId(int clientId) throws DAOException {
        List<Account> accounts = new ArrayList<>();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_ACCOUNTS_BY_CLIENT_ID);
            st.setInt(1, clientId);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Account account = initAccount(resultSet);
                accounts.add(account);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method takeAccountsByClientId: " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return accounts;
    }

    /**
     * Method that takes all active accounts of client that id equals clientId
     * @param clientId
     * @return list of accounts
     * @throws DAOException
     */

    public List<Account> takeActiveAccountsByClientId(int clientId) throws DAOException {
        List<Account> activeAccounts = new ArrayList<>();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_ACTIVE_ACCOUNTS_BY_CLIENT_ID);
            st.setInt(1, clientId);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Account activeAccount = initAccount(resultSet);
                activeAccounts.add(activeAccount);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method takeActiveAccountsByClientId: " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return activeAccounts;
    }

    /**
     * Method that takes all requests for connection to tariffs
     * @return list of requests for connection to tariffs
     * @throws DAOException
     */

    public List<TariffAddRequest> takeAllTariffAddRequests() throws DAOException {
        List<TariffAddRequest> tariffAddRequests = new ArrayList<>();
        ProxyConnection cn = null;
        Statement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.createStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_TARIFF_ADD_REQUESTS);
            while (resultSet.next()) {
                TariffAddRequest tariffAddRequest = initTariffAddRequest(resultSet);
                tariffAddRequests.add(tariffAddRequest);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method takeAllTariffAddRequests(): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return tariffAddRequests;
    }

    /**
     * Method that takes all accounts that satisfy sql query
     * @param query
     * @return list of accounts
     * @throws DAOException
     */

    private List<Account> takeAccountsByQuery (String query) throws DAOException{
        List<Account> accounts = new ArrayList<>();
        ProxyConnection cn = null;
        Statement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.createStatement();
            ResultSet resultSet = st.executeQuery(query);
            while (resultSet.next()) {
                Account account = initAccount(resultSet);
                accounts.add(account);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method takeAccountsByQuery(String query): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return accounts;
    }

    /**
     * Method that inserts request for connection to tariffs in DB table 'tariff_add_requests'
     * @param clientId
     * @param tariffId
     * @param address
     * @return boolean result
     * @throws DAOException
     */

    public boolean insertRequestAddTariff(int clientId, int tariffId, String address) throws DAOException {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(SQL_INSERT_TARIFF_ADD_REQUEST);
            stm.setInt(1, clientId);
            stm.setInt(2, tariffId);
            stm.setString(3, address);
            stm.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method insertRequestAddTariff()" + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that creates new account.
     * At first it deletes request for connection to tariff from table 'tariff_add_requests'.
     * And then it inserts new account in table 'accounts'.
     * @param requestId
     * @return boolean result
     * @throws DAOException
     */

    public boolean createAccount(int requestId) throws DAOException {
        boolean res = false;
        TariffAddRequest request = new TariffAddRequest();
        ProxyConnection cn = null;
        PreparedStatement stmTakeRequest = null;
        PreparedStatement stmDeleteRequest = null;
        PreparedStatement stmAddAccount = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            cn.setAutoCommit(false);
            stmTakeRequest = cn.prepareStatement(SQL_SELECT_TARIFF_ADD_REQUEST_BY_ID);
            stmTakeRequest.setInt(1, requestId);
            ResultSet resultSet = stmTakeRequest.executeQuery();
            resultSet.next();
            request = initTariffAddRequest(resultSet);

            stmDeleteRequest = cn.prepareStatement(SQL_DELETE_TARIFF_ADD_REQUEST_BY_ID);
            stmDeleteRequest.setInt(1, requestId);
            stmDeleteRequest.executeUpdate();

            stmAddAccount = cn.prepareStatement(SQL_INSERT_ACCOUNT);
            stmAddAccount.setInt(1, request.getClientId());
            stmAddAccount.setInt(2, request.getTariffId());
            stmAddAccount.setInt(3, request.getTariffId());
            stmAddAccount.setString(4, request.getAddress());
            stmAddAccount.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
                throw new DAOException("Exception in method createAccount" + e);
        } finally {
            try {
                if (cn != null) {
                    if (res) {
                        cn.commit();
                    } else {
                        cn.rollback();
                    }
                    cn.setAutoCommit(true);
                }
            } catch (SQLException e) {
                LOG.error("setAutoCommit(true) is failed in method createAccount()");
            }
            closeStatement(stmTakeRequest);
            closeStatement(stmDeleteRequest);
            closeStatement(stmAddAccount);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that takes all requests for connection to tariffs by client that id equals clientId
     * @param clientId
     * @return list of requests for connection to tariffs
     * @throws DAOException
     */

    public List<TariffAddRequest> takeAddTariffRequestsByClient(int clientId) throws DAOException {
        List<TariffAddRequest> tariffAddRequests = new ArrayList<>();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_TARIFF_ADD_REQUEST_BY_CLIENT);
            st.setInt(1, clientId);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                TariffAddRequest tariffAddRequest = initTariffAddRequest(resultSet);
                tariffAddRequests.add(tariffAddRequest);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method takeAddTariffRequestsByClient(): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return tariffAddRequests;
    }

    /**
     * Method that subtracts abonent payment from account that id equals accountId
     * @param accountId
     * @return boolean result
     * @throws DAOException
     */

    public boolean subtractAbonentPayment(int accountId) throws DAOException {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement stmPay = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();

            stm = cn.prepareStatement(SQL_SELECT_ABONENT_PAYMENT_BY_ACCOUNT_ID);
            stm.setInt(1, accountId);
            ResultSet resultSet = stm.executeQuery();
            resultSet.next();
            BigDecimal payment = new BigDecimal(resultSet.getString("tariffs.month_payment"));

            stmPay = cn.prepareStatement(SQL_UPDATE_SUB_BALANCE_BY_ACCOUNT_ID);
            stmPay.setBigDecimal(1, payment);
            stmPay.setInt(2, accountId);
            stmPay.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method subtractFromBalance" + e);
        } finally {
            closeStatement(stmPay);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that deletes request for connection to tariff from table 'tariff_add_requests'
     * @param requestId
     * @return boolean result
     * @throws DAOException
     */

    public boolean deleteTariffAddRequest(int requestId) throws DAOException {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(SQL_DELETE_TARIFF_ADD_REQUEST_BY_ID);
            stm.setInt(1, requestId);
            stm.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method deleteTariffAddRequest()" + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that close account that id equals accountId.
     * Closing account means setting date at field 'end_date' in table 'accounts' to this account.
     * @param accountId
     * @return boolean value
     * @throws DAOException
     */

    public boolean closeAccount (int accountId) throws DAOException {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_CLOSE_ACCOUNT);
            st.setInt(1, accountId);
            st.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method closeAccount()" + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that creates entity object 'Account' based on data contained in resultSet
     * @param resultSet
     * @return object Account
     * @throws SQLException
     */

    private Account initAccount (ResultSet resultSet) throws SQLException{
        Account account = new Account();
        account.setId(resultSet.getInt("accounts.account_id"));
        account.setClientId(resultSet.getInt("accounts.client_id"));
        account.setCreatingDate(resultSet.getDate("accounts.creating_date"));
        account.setEndDate(resultSet.getDate("accounts.end_date"));
        account.setBalance(resultSet.getBigDecimal("accounts.balance"));
        account.setTariffId(resultSet.getInt("accounts.tariff_id"));
        account.setAddress(resultSet.getString("accounts.address"));
        account.setBan(resultSet.getBoolean("accounts.ban"));
        account.setTariffName(resultSet.getString("tariffs.tariff_name"));
        account.setAbonentPayment(resultSet.getBigDecimal("tariffs.month_payment"));
        return account;
    }

    /**
     * Method that creates entity object 'TariffAddRequest' based on data contained in resultSet
     * @param resultSet
     * @return object TariffAddRequest
     * @throws SQLException
     */

    private TariffAddRequest initTariffAddRequest (ResultSet resultSet) throws SQLException{
        TariffAddRequest tariffAddRequest = new TariffAddRequest();
        tariffAddRequest.setId(resultSet.getInt("tariff_add_requests.request_id"));
        tariffAddRequest.setClientId(resultSet.getInt("tariff_add_requests.client_id"));
        tariffAddRequest.setTariffId(resultSet.getInt("tariff_add_requests.tariff_id"));
        tariffAddRequest.setTariffName(resultSet.getString("tariffs.tariff_name"));
        tariffAddRequest.setAddress(resultSet.getString("tariff_add_requests.address"));
        return tariffAddRequest;
    }
}
