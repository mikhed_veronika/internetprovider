package com.epam.provider.dao;

import com.epam.provider.connect.ConnectionPool;
import com.epam.provider.connect.ProxyConnection;
import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.ConnectionPoolException;
import com.epam.provider.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains sql queries.
 * Provides methods that get data and insert data in table 'clients' and 'administrators' in DB
 * @author Veronika Mikhed
 * @see User
 */

public class UserDAO extends AbstractDAO<String, User> {

    private static Logger LOG = Logger.getLogger(UserDAO.class);

    public static final String SQL_SELECT_ALL_USERS = "SELECT id, first_name, last_name, passport, address, phone_number, email, " +
            "password, role FROM " +
            "(SELECT client_id as 'id', first_name, last_name, passport, address, phone_number, email, password, \"client\" as role " +
            "FROM clients UNION" +
            "SELECT admin_id as 'id', first_name, last_name, passport, address, phone_number, email, password, " +
            "\"admin\" AS role FROM administrators) AS users";

    public static final String SQL_SELECT_USER_BY_EMAIL = "SELECT id, first_name, last_name, passport, address, phone_number, email, " +
            "password, role FROM (SELECT client_id as 'id', first_name, last_name, passport, address, phone_number, email, " +
            "password, \"client\" as role FROM clients UNION SELECT admin_id as 'id', first_name, last_name, passport, address, " +
            "phone_number, email, password, \"admin\" AS role FROM administrators) AS users WHERE email=?";

    public static final String SQL_SELECT_USER_BY_EMAIL_PASSWORD = "SELECT id, first_name, last_name, passport, address, phone_number, email, " +
            "password, role FROM (SELECT client_id as 'id', first_name, last_name, passport, address, phone_number, email, " +
            "password, \"client\" as role FROM clients UNION SELECT admin_id as 'id', first_name, last_name, passport, address, " +
            "phone_number, email, password, \"admin\" AS role FROM administrators) AS users WHERE email=? AND password=?";

    public static final String SQL_UPDATE_PASSWORD_BY_CLIENT_ID = "UPDATE clients SET password=? WHERE client_id=?";

    public static final String SQL_UPDATE_PASSWORD_BY_ADMIN_ID = "UPDATE administrators SET password=? WHERE admin_id=?";

    /**
     * Method that takes all clients and admins from tables 'clients' and 'administrators'
     * @return list of all users
     * @throws DAOException
     */

    @Override
    public List<User> findAll() throws DAOException {
        List<User> users = new ArrayList<>();
        ProxyConnection cn = null;
        Statement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.createStatement();
            ResultSet rs = st.executeQuery(SQL_SELECT_ALL_USERS);
            while (rs.next()) {
                User user;
                user = initUser(rs);
                users.add(user);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method findAll(): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return users;
    }

    /**
     * Method that takes user from table 'clients' or 'administrators' by email
     * @param key is email
     * @return entity User
     * @throws DAOException
     */

    @Override
    public User findEntityByKey(String key) throws DAOException {
        User user;
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_USER_BY_EMAIL);
            st.setString(1, key);
            ResultSet rs = st.executeQuery();
            rs.next();
            user = initUser(rs);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("SQL exception in method findEntityByKey(): " + e);
        } finally {
            closeStatement(st);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return user;
    }

    /**
     * Method that provides authentication of user by email and password
     * @param email
     * @param password
     * @return User
     * @throws DAOException
     */

    public User authenticate(String email, String password) throws DAOException{
        User user = null;
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(SQL_SELECT_USER_BY_EMAIL_PASSWORD);
            stm.setString(1, email);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                user = initUser(rs);
            } else {
                LOG.info("Authentication failed: Incorrect email or password");
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method authenticate " + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return user;
    }

    /**
     * Method that creates entity User based on data contained in resultSet
     * @param resultSet
     * @return object User
     * @throws SQLException
     */

    private User initUser (ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setPassport(resultSet.getString("passport"));
        user.setAddress(resultSet.getString("address"));
        user.setPhoneNumber(resultSet.getString("phone_number"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        user.setRole(Role.valueOf(resultSet.getString("role").toUpperCase()));
        return user;
    }

    /**
     * Method that changes password of user that id equals userId
     * @param userId
     * @param newPassword
     * @param role
     * @return boolean result
     * @throws DAOException
     */

    public boolean changePassword (int userId, String newPassword, Role role) throws DAOException{
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            if (role == Role.ADMIN) {
                stm = cn.prepareStatement(SQL_UPDATE_PASSWORD_BY_ADMIN_ID);
            }
            else {
                stm = cn.prepareStatement(SQL_UPDATE_PASSWORD_BY_CLIENT_ID);
            }
            stm.setString(1, newPassword);
            stm.setInt(2, userId);
            stm.executeUpdate();
            res = true;
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method changePassword" + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }

    /**
     * Method that checks if the password received as parameter equals password of user that email equals email received as parameter
     * @param password
     * @param email
     * @return boolean result
     * @throws DAOException
     */

    public boolean checkPassword (String password, String email) throws DAOException {
        boolean res = false;
        ProxyConnection cn = null;
        PreparedStatement stm = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            stm = cn.prepareStatement(SQL_SELECT_USER_BY_EMAIL);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                String passFromDb = rs.getString("password");
                if (password != null && password.equals(passFromDb)) {
                    res = true;
                }
            } else {
                LOG.error("Error in checkPassword(): no user in db with such email");
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Exception in method checkPassword " + e);
        } finally {
            closeStatement(stm);
            ConnectionPool.getInstance().returnConnection(cn);
        }
        return res;
    }
}
