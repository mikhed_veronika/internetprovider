package com.epam.provider.exception;

/**
 * Exception in DAO layer
 * @author Veronika Mikhed
 */

public class DAOException extends Exception{
    public DAOException() {
    }

    public DAOException(String message, Throwable exception) {
        super(message, exception);
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(Throwable exception) {
        super(exception);
    }
}
