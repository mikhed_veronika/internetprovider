package com.epam.provider.entity;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Simple Account entity that contains getters and setters
 * @author Veronika Mikhed
 */

public class Account extends Entity {

    private int clientId;
    private Date creatingDate;
    private Date endDate;
    private BigDecimal balance;
    private BigDecimal abonentPayment;
    private int tariffId;
    private String tariffName;
    private String address;
    private boolean ban;

    public Account() { }

    public Account(int id, int clientId, Date creatingDate, Date endDate, BigDecimal balance,
                   int tariffId, String tariffName, String address, boolean ban, BigDecimal abonentPayment) {
        super(id);
        this.clientId = clientId;
        this.creatingDate = creatingDate;
        this.endDate = endDate;
        this.balance = balance;
        this.tariffId = tariffId;
        this.tariffName = tariffName;
        this.address = address;
        this.ban = ban;
        this.abonentPayment = abonentPayment;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getTariffId() {
        return tariffId;
    }

    public void setTariffId(int tariffId) {
        this.tariffId = tariffId;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isBan() {
        return ban;
    }

    public void setBan(boolean ban) {
        this.ban = ban;
    }

    public BigDecimal getAbonentPayment() {
        return abonentPayment;
    }

    public void setAbonentPayment(BigDecimal abonentPayment) {
        this.abonentPayment = abonentPayment;
    }

    @Override
    public String toString() {
        return "Account{" +
                "clientId=" + clientId +
                ", creatingDate=" + creatingDate +
                ", endDate=" + endDate +
                ", balance=" + balance +
                ", abonentPayment=" + abonentPayment +
                ", tariffId=" + tariffId +
                ", tariffName='" + tariffName + '\'' +
                ", address='" + address + '\'' +
                ", ban=" + ban +
                '}';
    }
}
