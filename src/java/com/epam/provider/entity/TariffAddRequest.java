package com.epam.provider.entity;

/**
 * Simple TariffAddRequest entity that contains getters and setters
 * @author Veronika Mikhed
 */

public class TariffAddRequest extends Entity{

    private int clientId;
    private int tariffId;
    private String tariffName;
    private String address;

    public TariffAddRequest() {}

    public TariffAddRequest(int id, int clientId, int tariffId, String tariffName, String address) {
        super(id);
        this.clientId = clientId;
        this.tariffId = tariffId;
        this.tariffName = tariffName;
        this.address = address;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getTariffId() {
        return tariffId;
    }

    public void setTariffId(int tariffId) {
        this.tariffId = tariffId;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "TariffAddRequest{" +
                "Id=" + getId() +
                ", clientId=" + clientId +
                ", tariffId=" + tariffId +
                ", tariffName='" + tariffName + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
