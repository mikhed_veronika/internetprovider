package com.epam.provider.entity;

/**
 * Enum with user's roles
 * @author Veronika Mikhed
 */
public enum  Role {
    CLIENT, ADMIN
}
