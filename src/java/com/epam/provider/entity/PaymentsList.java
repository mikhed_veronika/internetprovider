package com.epam.provider.entity;

import java.util.ArrayList;

/**
 * Simple PaymentsList entity that contains getters and setters
 * @author Veronika Mikhed
 * @see PaymentInfo
 */

public class PaymentsList {

    private int accountId;
    private ArrayList<PaymentInfo> paymentsList;

    public PaymentsList() {}

    public PaymentsList (int accountId) {
        this.accountId = accountId;
        this.paymentsList = new ArrayList<PaymentInfo>();
    }

    public PaymentsList(int accountId, ArrayList<PaymentInfo> paymentsList) {
        this.accountId = accountId;
        this.paymentsList = paymentsList;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public ArrayList<PaymentInfo> getPaymentsList() {
        return paymentsList;
    }

    public void setPaymentsList(ArrayList<PaymentInfo> paymentsList) {
        this.paymentsList = paymentsList;
    }

    public void addPaymentInfo (PaymentInfo info) {
        if (this.paymentsList == null) {
            this.paymentsList = new ArrayList<PaymentInfo>();
        }
        paymentsList.add(info);
    }
}
