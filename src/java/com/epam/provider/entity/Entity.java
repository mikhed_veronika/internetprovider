package com.epam.provider.entity;

/**
 * Abstract Entity that contains only field id
 * @author Veronika Mikhed
 */

public abstract class Entity {

    private int id;

    public Entity() {}

    public Entity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
