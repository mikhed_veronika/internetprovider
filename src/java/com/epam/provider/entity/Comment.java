package com.epam.provider.entity;

import java.sql.Date;

/**
 * Simple Comment entity that contains getters and setters
 * @author Veronika Mikhed
 */

public class Comment extends Entity {

    private int clientId;
    private String firstName;
    private String lastName;
    private String header;
    private String text;
    private Date dateTime;

    public Comment() {}

    public Comment(int id, int clientId, String firstName, String lastName, String header, String text, Date dateTime) {
        super(id);
        this.clientId = clientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.header = header;
        this.text = text;
        this.dateTime = dateTime;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + getId() +
                "clientId=" + clientId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", header='" + header + '\'' +
                ", text='" + text + '\'' +
                ", dateTime=" + dateTime +
                '}';
    }
}
