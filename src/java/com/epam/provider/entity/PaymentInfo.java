package com.epam.provider.entity;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Simple PaymentInfo entity that contains getters and setters
 * @author Veronika Mikhed
 */

public class PaymentInfo {

    private long paymentId;
    private Date paymentDate;
    private BigDecimal paymentSum;

    public PaymentInfo() {}

    public PaymentInfo(long paymentId) {
        this.paymentId = paymentId;
    }

    public PaymentInfo(long paymentId, Date paymentDate, BigDecimal paymentSum) {
        this.paymentId = paymentId;
        this.paymentDate = paymentDate;
        this.paymentSum = paymentSum;
    }

    public long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public BigDecimal getPaymentSum() {
        return paymentSum;
    }

    public void setPaymentSum(BigDecimal paymentSum) {
        this.paymentSum = paymentSum;
    }
}
