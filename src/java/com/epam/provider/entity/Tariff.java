package com.epam.provider.entity;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Simple Tariff entity that contains getters and setters
 * @author Veronika Mikhed
 */

public class Tariff extends Entity{

    private String tariffName;
    private String description;
    private String currencyCode;
    private BigDecimal monthPayment;
    private double uploadSpeed;
    private double downloadSpeed;
    private Date startDate;
    private Date finishDate;
    private double trafficVolume;

    public Tariff() {}

    public Tariff(int id, String tariffName, String description, String currencyCode, BigDecimal monthPayment, double uploadSpeed,
                  double downloadSpeed, Date startDate, Date finishDate, double trafficVolume) {
        super(id);
        this.tariffName = tariffName;
        this.description = description;
        this.currencyCode = currencyCode;
        this.monthPayment = monthPayment;
        this.uploadSpeed = uploadSpeed;
        this.downloadSpeed = downloadSpeed;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.trafficVolume = trafficVolume;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getMonthPayment() {
        return monthPayment;
    }

    public void setMonthPayment(BigDecimal monthPayment) {
        this.monthPayment = monthPayment;
    }

    public double getUploadSpeed() {
        return uploadSpeed;
    }

    public void setUploadSpeed(double uploadSpeed) {
        this.uploadSpeed = uploadSpeed;
    }

    public double getDownloadSpeed() {
        return downloadSpeed;
    }

    public void setDownloadSpeed(double downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public double getTrafficVolume() {
        return trafficVolume;
    }

    public void setTrafficVolume(double trafficVolume) {
        this.trafficVolume = trafficVolume;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "id='" + getId()  +
                "tariffName='" + tariffName + '\'' +
                ", description='" + description + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", monthPayment=" + monthPayment +
                ", uploadSpeed=" + uploadSpeed +
                ", downloadSpeed=" + downloadSpeed +
                ", startDate=" + startDate +
                ", finishDate=" + finishDate +
                ", trafficVolume=" + trafficVolume +
                '}';
    }
}
