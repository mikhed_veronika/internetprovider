package com.epam.provider.servlet;

import com.epam.provider.command.Command;
import com.epam.provider.command.RequestHelper;
import com.epam.provider.connect.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main Servlet of the application
 * @author Veronika Mikhed
 */
@WebServlet("/controller")
public class ProviderServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(ProviderServlet.class);

    public ProviderServlet() {
        super();
    }

    /**
     * Calls processRequest method
     * @see ProviderServlet#processRequest(HttpServletRequest request, HttpServletResponse response)
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
        logger.info("GET");
    }

    /**
     * Calls processRequest method
     * @see ProviderServlet#processRequest(HttpServletRequest request, HttpServletResponse response)
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
        logger.info("POST");
    }

    /**
     * Executes commands and do forward or sendRedirect depending on the request's attribute ATTR_PAGE value
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Command command = RequestHelper.getInstance().getCommand(request);
        String page = command.execute(request, response);
        if (request.getAttribute(Command.ATTR_PAGE) == Command.REDIRECT_PAGE) {
            response.sendRedirect(page);
        }
        else if (request.getAttribute(Command.ATTR_PAGE) == Command.FORWARD_PAGE) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        }
    }

    /**
     * Destroys Connection Pool
     */

    @Override
    public void destroy() {
        ConnectionPool.getInstance().destroyPool();
    }
}