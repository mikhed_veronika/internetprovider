package com.epam.provider.service;

import com.epam.provider.dao.PaymentsDAO;
import com.epam.provider.entity.PaymentInfo;
import com.epam.provider.entity.PaymentsList;
import com.epam.provider.exception.DAOException;
import com.epam.provider.exception.ServiceException;

import java.math.BigDecimal;

/**
 * Class of the service layer that provides operations with payments
 * @author Veronika Mikhed
 * @see PaymentsDAO
 * @see PaymentsList
 * @see PaymentInfo
 */

public class PaymentService {

    public static PaymentsDAO paymentsDAO = new PaymentsDAO();

    /**
     * Method that takes all payments by account that id equals accountId
     * @param accountId
     * @return paymentsList
     * @throws ServiceException
     */

    public PaymentsList takePaymentsListByAccountId (int accountId) throws ServiceException {
        PaymentsList paymentsList = new PaymentsList();
        try {
            paymentsList = paymentsDAO.takePaymentsListByAccountId(accountId);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return paymentsList;
    }

    /**
     * Method that increases balance of account that id equals accountId
     * @param accountId
     * @param summa
     * @param cardNumber
     * @param cvv2
     * @return boolean result
     * @throws ServiceException
     */

    public boolean addMoneyToAccount(int accountId, BigDecimal summa, String cardNumber, String cvv2) throws ServiceException {
        boolean res = false;
        try {
            res = paymentsDAO.addMoneyToAccount(accountId, summa, cardNumber, cvv2);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return res;
    }

}
