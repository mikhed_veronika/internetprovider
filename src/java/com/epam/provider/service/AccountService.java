package com.epam.provider.service;

import com.epam.provider.dao.AccountDAO;
import com.epam.provider.entity.Account;
import com.epam.provider.entity.TariffAddRequest;
import com.epam.provider.exception.DAOException;
import com.epam.provider.exception.ServiceException;

import java.util.List;

/**
 * Class of the service layer that provides operations with client's accounts
 * @author Veronika Mikhed
 * @see Account
 * @see TariffAddRequest
 * @see AccountDAO
 */
public class AccountService {

    public static AccountDAO accountDAO = new AccountDAO();

    /**
     * Method that creates accounts by requests list
     * @param listRequests
     * @throws ServiceException
     */

    public void createAccounts (String[] listRequests) throws ServiceException {
        try {
            for (String s : listRequests) {
                accountDAO.createAccount(Integer.valueOf(s));
            }
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }

    /**
     * Method that closes account by accountId
     * @param accountId
     * @return booleanresult
     * @throws ServiceException
     */

    public boolean closeAccount (int accountId) throws ServiceException {
        boolean res = false;
        try {
            res = accountDAO.closeAccount(accountId);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return res;
    }

    /**
     * Method that adds request for connection to tariff in table tariff_add_requests
     * @param clientId
     * @param tariffId
     * @param address
     * @return boolean result
     * @throws ServiceException
     */

    public boolean insertTariffAddRequest (int clientId, int tariffId, String address) throws ServiceException {
        boolean res = false;
        try {
            res = accountDAO.insertRequestAddTariff(clientId, tariffId, address);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return  res;
    }

    /**
     * Method that deletes request for connection to tariff from table tariff_add_requests
     * @param listRequests
     * @throws ServiceException
     */

    public void deleteTariffAddRequests (String[] listRequests) throws ServiceException {
        try {
            for (String s : listRequests) {
                accountDAO.deleteTariffAddRequest(Integer.valueOf(s));
            }
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }

    /**
     * Method that takes all active accounts of client with id equals clientId
     * @param clientId
     * @return accounts
     * @throws ServiceException
     */

    public List<Account> takeActiveAccountsByClientId (int clientId) throws ServiceException {
        List<Account> accounts;
        try {
            accounts = accountDAO.takeActiveAccountsByClientId(clientId);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return accounts;
    }

    /**
     * Method that takes all requests for connection to tariffs of client with id equals clientId
     * @param clientId
     * @return tariffAddRequests
     * @throws ServiceException
     */

    public List<TariffAddRequest> takeAddTariffRequestsByClientId (int clientId) throws ServiceException {
        List<TariffAddRequest> tariffAddRequests;
        try {
            tariffAddRequests = accountDAO.takeAddTariffRequestsByClient(clientId);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return tariffAddRequests;
    }

    /**
     * Method that takes all accounts of client with id equals clientId
     * @param clientId
     * @return accounts
     * @throws ServiceException
     */

    public List<Account> takeAccountsByClientId (int clientId) throws ServiceException {
        List<Account> accounts;
        try {
            accounts = accountDAO.takeAccountsByClientId(clientId);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return accounts;
    }

    /**
     * Method that takes list of accounts which can be unbanned
     * @return accountsToUnban
     * @throws ServiceException
     */

    public List<Account> takeAccountsToUnban() throws ServiceException{
        List<Account> accountsToUnban;
        try {
            accountsToUnban = accountDAO.findAccountsToUnban();
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return accountsToUnban;
    }

    /**
     * Method that takes list of accounts which can be banned
     * @return accountsToBan
     * @throws ServiceException
     */

    public List<Account> takeAccountsToBan() throws ServiceException {
        List<Account> accountsToBan;
        try {
            accountsToBan = accountDAO.findAccountsToBan();
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return accountsToBan;
    }


    /**
     * Method that unban all accounts from list
     * @param listToUnban
     * @throws ServiceException
     */

    public void unbanUsersByAccountId(String[] listToUnban) throws ServiceException {
        if (listToUnban!=null) {
            try {
                for (String s : listToUnban) {
                    accountDAO.setBan(Integer.valueOf(s), false);
                }
            } catch (DAOException e) {
                throw  new ServiceException();
            }
        }

    }

    /**
     * Method that ban all accounts by list
     * @param listToBan
     * @throws ServiceException
     */

    public void banUsersByAccountId(String[] listToBan) throws ServiceException {
        try {
            for(String s : listToBan) {
                accountDAO.setBan(Integer.valueOf(s), true);
            }
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }

    /**
     * Method that takes all requests for connection to tariffs
     * @return tariffAddRequests
     * @throws ServiceException
     */

    public List<TariffAddRequest> takeAllTariffAddRequests () throws ServiceException {
        List<TariffAddRequest> tariffAddRequests;
        try {
            tariffAddRequests = accountDAO.takeAllTariffAddRequests();
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return tariffAddRequests;
    }
}
