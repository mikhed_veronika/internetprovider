package com.epam.provider.service;

import com.epam.provider.dao.TariffDAO;
import com.epam.provider.entity.Tariff;
import com.epam.provider.exception.DAOException;
import com.epam.provider.exception.ServiceException;

import java.math.BigDecimal;
import java.util.List;

/**
 * Class of the service layer that provides operations with tariff plans
 * @author Veronika Mikhed
 * @see TariffDAO
 * @see Tariff
 */

public class TariffService {

    public static TariffDAO tariffDAO = new TariffDAO();

    /**
     * Method that takes all tariffs (includes active and closed tariffs)
     * @return tariffList
     * @throws ServiceException
     */

    public List<Tariff> takeAllTariffs() throws ServiceException{
        List<Tariff> tariffList;
        try {
            tariffList = tariffDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return tariffList;
    }

    /**
     * Method that takes list of all active tariffs
     * @return tariffList
     * @throws ServiceException
     */

    public List<Tariff> takeAllActiveTariffsList() throws ServiceException{
        List<Tariff> tariffList;
        try {
            tariffList = tariffDAO.findAllActiveTariffs();
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return tariffList;
    }

    /**
     * Method that takes list of all closed tariffs
     * @return tariffList
     * @throws ServiceException
     */

    public List<Tariff> takeAllClosedTariffsList() throws ServiceException{
        List<Tariff> tariffList;
        try {
            tariffList = tariffDAO.findAllClosedTariffs();
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return tariffList;
    }

    /**
     * Method that closes tariff plan which id equals tariffId
     * @param tariffId
     * @return boolean result
     * @throws ServiceException
     */

    public boolean closeTariffById (int tariffId) throws ServiceException {
        boolean res = false;
        try {
            res = tariffDAO.closeTariff(tariffId);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return res;
    }

    /**
     * Method that insert new tariff plan in table 'tariffs'
     * @param tariffName
     * @param tariffDescription
     * @param currencyCode
     * @param monthPayment
     * @param uploadSpeed
     * @param downloadSpeed
     * @param trafficVolume
     * @return boolean result
     * @throws ServiceException
     */

    public boolean insertTariff (String tariffName, String tariffDescription, String currencyCode, BigDecimal monthPayment,
                                 Double uploadSpeed, Double downloadSpeed, Double trafficVolume) throws ServiceException{
        boolean res = false;
        try {
            res = tariffDAO.insertTariff(tariffName, tariffDescription, currencyCode, monthPayment, uploadSpeed, downloadSpeed, trafficVolume);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return res;
    }

    /**
     * Method that changes tariff plan which id equals tariffId
     * @param tariffId
     * @param description
     * @param currencyCode
     * @param monthPayment
     * @param uploadSpeed
     * @param downloadSpeed
     * @param trafficVolume
     * @return boolean result
     * @throws ServiceException
     */

    public boolean changeTariff(int tariffId, String description, String currencyCode, BigDecimal monthPayment, Double uploadSpeed,
                                Double downloadSpeed, Double trafficVolume) throws ServiceException {
        boolean res = false;
        try {
            res = tariffDAO.changeTariff(tariffId, description, currencyCode, monthPayment, uploadSpeed, downloadSpeed, trafficVolume);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return res;
    }
}
