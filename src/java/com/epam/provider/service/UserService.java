package com.epam.provider.service;


import com.epam.provider.dao.UserDAO;
import com.epam.provider.entity.Role;
import com.epam.provider.entity.User;
import com.epam.provider.exception.DAOException;
import com.epam.provider.exception.ServiceException;

/**
 * Class of the service layer that provides operations with users (including clients and administrators)
 * @author Veronika Mikhed
 * @see UserDAO
 * @see User
 */

public class UserService {

    public static UserDAO userDAO = new UserDAO();

    /**
     * Method that provides authentication of user
     * @param email
     * @param password
     * @return user
     * @throws ServiceException
     */

    public User authenticate(String email, String password) throws ServiceException {
        User user;
        try {
            user = userDAO.authenticate(email, password);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return user;
    }

    /**
     * Method that checks if password entered by user equals real password
     * @param currPassword
     * @param email
     * @return boolean result
     * @throws ServiceException
     */

    public boolean checkPassword (String currPassword, String email) throws ServiceException {
        boolean res = false;
        try {
            res = userDAO.checkPassword(currPassword, email);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return res;
    }

    /**
     * Method that changes password of user that id equals userId
     * @param userId
     * @param newPassword
     * @param role
     * @return boolean result
     * @throws ServiceException
     */

    public boolean changePassword (int userId, String newPassword, Role role) throws ServiceException {
        boolean res = false;
        try {
            res = userDAO.changePassword(userId, newPassword, role);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return res;
    }
}
