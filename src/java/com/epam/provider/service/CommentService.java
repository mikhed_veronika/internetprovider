package com.epam.provider.service;


import com.epam.provider.dao.CommentDAO;
import com.epam.provider.entity.Comment;
import com.epam.provider.exception.DAOException;
import com.epam.provider.exception.ServiceException;

import java.util.List;

/**
 * Class of the service layer that provides operations with comments
 * @author Veronika Mikhed
 * @see CommentDAO
 * @see Comment
 */
public class CommentService {

    public static CommentDAO commentDAO = new CommentDAO();

    /**
     * Method that takes all user's commentaries
     * @return allComments
     * @throws ServiceException
     */

    public List<Comment> takeAllComments() throws ServiceException {
        List<Comment> allComments;
        try {
            allComments = commentDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return allComments;
    }

    /**
     * Method that deletes commentaries from list by id
     * @param listToDelete
     * @throws ServiceException
     */

    public void deleteComments (String[] listToDelete) throws ServiceException {
        try {
            for (String s : listToDelete) {
                commentDAO.deleteComment(Integer.valueOf(s));
            }
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }

    /**
     * Method that inserts a comment in table 'comments'
     * @param clientId
     * @param commentHeader
     * @param commentText
     * @return boolean result
     * @throws ServiceException
     */

    public boolean insertComment(int clientId, String commentHeader, String commentText) throws ServiceException {
        boolean res = false;
        try {
            res = commentDAO.insertComment(clientId, commentHeader, commentText);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return res;
    }

}
