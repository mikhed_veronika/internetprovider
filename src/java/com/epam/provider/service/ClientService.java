package com.epam.provider.service;

import com.epam.provider.dao.ClientDAO;
import com.epam.provider.exception.DAOException;
import com.epam.provider.exception.ServiceException;

/**
 * Class of the service layer that provides operations with clients
 * @author Veronika Mikhed
 * @see ClientDAO
 */

public class ClientService {

    public static ClientDAO clientDAO = new ClientDAO();

    /**
     * Method that adds new client in table 'clients'
     * @param firstName
     * @param lastName
     * @param passport
     * @param address
     * @param phone
     * @param email
     * @param password
     * @return boolean result
     * @throws ServiceException
     */

    public boolean addNewClient (String firstName, String lastName, String passport, String address,
                                 String phone, String email, String password) throws ServiceException {
        boolean res = false;
        try {
            res = clientDAO.insertClient(firstName, lastName, passport, address, phone, email, password);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return res;
    }

    /**
     * Method that takes email of client with id equals clientId
     * @param clientId
     * @return email
     * @throws ServiceException
     */
    public String takeEmailByClientId (int clientId) throws ServiceException {
        String email;
        try {
            email = clientDAO.findEmailByClientId(clientId);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return email;
    }
}
