package com.epam.provider.manager;

import java.util.ResourceBundle;

/**
 * Contains pages paths
 * @author Veronika Mikhed
 */
public class ConfigurationManager {

    private static ConfigurationManager instance;
    private ResourceBundle resourceBundle;
    private static final String BUNDLE_NAME = "config";

    public static final String LOGIN_PAGE_PATH = "LOGIN_PAGE_PATH";
    public static final String TARIFFS_PAGE_PATH = "TARIFFS_PAGE_PATH";
    public static final String COMMENTS_PAGE_PATH = "COMMENTS_PAGE_PATH";

    public static final String MAIN_PAGE_PATH = "MAIN_PAGE_PATH";
    public static final String MAIN_CLIENT_PAGE_PATH = "MAIN_CLIENT_PAGE_PATH";
    public static final String MAIN_ADMIN_PAGE_PATH = "MAIN_ADMIN_PAGE_PATH";


    public static final String CLIENT_PROFILE_PAGE_PATH = "CLIENT_PROFILE_PAGE_PATH";
    public static final String CLIENT_CREATING_ACCOUNT_PAGE_PATH = "CLIENT_CREATING_ACCOUNT_PAGE_PATH";
    public static final String CLIENT_TARIFFS_PAGE_PATH = "CLIENT_TARIFFS_PAGE_PATH";
    public static final String CLIENT_COMMENTS_PAGE_PATH = "CLIENT_COMMENTS_PAGE_PATH";
    public static final String CLIENT_PAYMENT_PAGE_PATH = "CLIENT_PAYMENT_PAGE_PATH";
    public static final String CLIENT_ADD_COMMENT_PAGE_PATH = "CLIENT_ADD_COMMENT_PAGE_PATH";
    public static final String CLIENT_CHANGE_PASSWORD_PAGE_PATH = "CLIENT_CHANGE_PASSWORD_PAGE_PATH";
    public static final String CLIENT_CLOSE_ACCOUNT_PAGE_PATH = "CLIENT_CLOSE_ACCOUNT_PAGE_PATH";
    public static final String CLIENT_ARCHIVE_TARIFFS_PAGE_PATH = "CLIENT_ARCHIVE_TARIFFS_PAGE_PATH";

    public static final String ADMIN_PROFILE_PAGE_PATH = "ADMIN_PROFILE_PAGE_PATH";
    public static final String ADMIN_TARIFFS_PAGE_PATH = "ADMIN_TARIFFS_PAGE_PATH";
    public static final String ADMIN_COMMENTS_PAGE_PATH = "ADMIN_COMMENTS_PAGE_PATH";
    public static final String ADMIN_CHANGE_PASSWORD_PAGE_PATH = "ADMIN_CHANGE_PASSWORD_PAGE_PATH";
    public static final String ADMIN_ADD_TARIFF_PAGE_PATH = "ADMIN_ADD_TARIFF_PAGE_PATH";
    public static final String ADMIN_HANDLE_REQUESTS_PAGE_PATH = "ADMIN_HANDLE_REQUESTS_PAGE_PATH";
    public static final String ADMIN_SET_BAN_PAGE_PATH = "ADMIN_SET_BAN_PAGE_PATH";
    public static final String ADMIN_SET_UNBAN_PAGE_PATH = "ADMIN_SET_UNBAN_PAGE_PATH";
    public static final String ADMIN_CLOSE_TARIFF_PAGE_PATH = "ADMIN_CLOSE_TARIFF_PAGE_PATH";
    public static final String ADMIN_CHANGE_TARIFF_PAGE_PATH = "ADMIN_CHANGE_TARIFF_PAGE_PATH";
    public static final String ADMIN_ARCHIVE_TARIFFS_PAGE_PATH = "ADMIN_ARCHIVE_TARIFFS_PAGE_PATH";

    public static final String ERROR_PAGE_PATH = "ERROR_PAGE_PATH";

    public static ConfigurationManager getInstance() {
        if (instance == null) {
            instance = new ConfigurationManager();
            instance.resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
        }
        return instance;
    }
    public String getProperty(String key) {
        return (String)resourceBundle.getObject(key);
    }
}
