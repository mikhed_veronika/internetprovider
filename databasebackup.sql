-- MySQL dump 10.13  Distrib 5.7.11, for Win64 (x86_64)
--
-- Host: localhost    Database: internet_provider
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `creating_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `balance` decimal(15,3) NOT NULL,
  `tariff_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `ban` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`account_id`),
  KEY `client_id_idx` (`client_id`),
  KEY `tariff_id_idx` (`tariff_id`),
  CONSTRAINT `client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tariff_id` FOREIGN KEY (`tariff_id`) REFERENCES `tariffs` (`tariff_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,1,'2015-12-01',NULL,355000.000,3,'Беларусь, г. Брест, ул. Ленина, 4-142','1'),(2,2,'2015-07-10','2015-10-10',0.000,1,'Беларусь, г. Минск, ул. Притыцкого, 21-18','0'),(3,2,'2016-01-15',NULL,100000.000,6,'Беларусь, г. Минск, ул. Академическая, 12-91','1'),(4,3,'2015-12-25',NULL,-50000.000,5,'Беларусь, г. Гомель, ул. Красная, 99-75','0'),(5,4,'2015-05-02','2015-10-15',0.000,2,'Беларусь, г. Брест, ул. Пушкина, 11-1','1'),(6,4,'2015-10-20',NULL,75000.000,4,'Беларусь, г. Минск, ул. Матусевича, 87-12','0'),(7,5,'2016-03-01','2016-06-18',272000.000,6,'Беларусь, г. Минск, ул. Глебки, 82-124','0'),(8,6,'2015-09-15',NULL,-120000.000,2,'Беларусь, г. Солигорск, ул. Октябрьская, 12А-61','0'),(9,1,'2016-05-09',NULL,-31000.000,4,'Беларусь, г. Минск, пр. Независимости, 101-20','0'),(10,5,'2016-06-10',NULL,310000.000,5,'Минск, ул. П. Глебки, 82-124','0'),(11,5,'2016-06-11',NULL,0.000,6,'г. Минск, пр. Независимости, 4','0'),(12,17,'2016-06-12',NULL,100000.000,7,'Беларусь, г. Минск, ул. газеты Правда, 58-16','0');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administrators`
--

DROP TABLE IF EXISTS `administrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrators` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `passport` varchar(9) NOT NULL,
  `phone_number` varchar(14) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrators`
--

LOCK TABLES `administrators` WRITE;
/*!40000 ALTER TABLE `administrators` DISABLE KEYS */;
INSERT INTO `administrators` VALUES (1,'Алла','Кузнецова','alla.kuznecova@gmail.com','631e36b8852130b605b4062f518b7196','PT9614752','+375298425698','Беларусь, г. Минск, ул. Харьковская, 67-21'),(2,'Алексей','Морозов','alexey.morozov@gmail.com','40cb9c4fafd06d222bdc9961f771e62f','KH3687458','+375296985645','Беларусь, г. Минск, пр. Независимости, 91-120'),(3,'Владимир','Васильев','vladimir.vasilev@gmail.com','cdb515b05256332821e1a005a0e9250a','TM1875541','+375339186891','Беларусь, г. Минск, ул. Маркса, 36-14'),(4,'Татьяна','Иванова','tatiana.ivanova@gmail.com','6b6a0fb0dff430f701e3594825a51af0','HK1258632','+375298621543','Беларусь, г. Минск, ул. Ольшевскогоб 81-171');
/*!40000 ALTER TABLE `administrators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `passport` varchar(9) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone_number` varchar(14) NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'Иван','Иванов','ivanov.ivan@gmail.com','4dfe6e220d16e7b633cfdd92bcc8050b','HM8512750','Беларусь, г. Брест, ул. Ленина, 4-142','+375291863574'),(2,'Петр','Петров','petrov.petr@gmail.com','f396c3b74762b1fee69b10abb875139b','TK6239852','Беларусь, г. Минск, ул. Академическая, 12-91','+375298036477'),(3,'Мария','Данилова','danilova.maria@mail.ru','6c91ac0330b9c28d3b9930c716d3c743','MH1247854','Беларусь, г. Гомель, ул. Красная, 99-75','+375335512681'),(4,'Григорий','Ермаков','ermakov.grigorii@tut.by','a54985f33a4609d33b9b2c9dc8806482','PK1865475','Беларусь, г. Минск, ул. Матусевича, 87-12','+375449654785'),(5,'Вероника','Михед','mikhed.veronika@gmail.com','a9cbcc987636314922d6e09c9ac4678c','MP3027318','Беларусь, г. Минск, ул. Глебки, 82-124','+375297422460'),(6,'Анна','Мельникова','melnikova.anna@gmail.com','5ecc665f7a60ea51c989310f7e4b891d','TA9567851','Беларусь, г. Солигорск, ул. Октябрьская, 12А-61','+375291876954'),(15,'Виктория','Михед','vika.mikhed@gmail.com','a9cbcc987636314922d6e09c9ac4678c','AB1569874','Беларусь, г. Минск, ул. П. Глебки, 82-124','+375298771387'),(17,'Григорий','Жук','zhuk.grisha@gmail.com','ce32faf8a2c784c94f0134d069e612af','AE3953234','Беларусь, г. Минск, ул. газеты Правда, 58-16','+375298745874');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `header` varchar(150) NOT NULL,
  `text` varchar(5000) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `client_id_idx` (`client_id`),
  KEY `id_client_idx` (`client_id`),
  CONSTRAINT `id_client` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (3,6,'Lorem ipsum dolor sit amet, consectetur adipisicing.','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur illum veniam fugit ipsa perspiciatis laborum molestias neque amet, ea vel qui quia voluptatum assumenda molestiae. Minus dicta, quae quam dolores maiores facere rerum culpa dolor id assumenda molestiae laborum debitis repudiandae, unde nostrum repellat aperiam. Itaque ea iusto ipsum officia, quos ipsam nulla blanditiis asperiores cum?','2016-01-20 10:16:01'),(4,2,'Lorem ipsum dolor sit amet, consectetur.','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium dicta dolorum atque eos saepe doloribus quisquam explicabo odio molestias error molestiae ad, rem repellat harum, excepturi! Nisi maxime temporibus sed hic quaerat, dolorem, doloribus! Eligendi neque et excepturi perferendis, alias eos id sunt minima fuga molestias labore nemo dolore maiores, magni rerum praesentium quis illum consectetur dolorem quam eius delectus facilis cumque? Quae laboriosam, autem error fugit maxime, omnis officiis.','2016-01-25 11:48:05'),(5,1,'Lorem ipsum dolor sit amet, consectetur adipisicing.','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores suscipit deleniti consectetur error illum mollitia magni voluptates, quod impedit id, numquam voluptatibus distinctio dolorem fugit quo aliquam, beatae veniam. Reiciendis, molestiae, libero. Fugit recusandae sunt totam nisi porro, autem, officia.','2016-02-03 22:14:41');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `payment_id` bigint(15) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `payment_date` datetime NOT NULL,
  `payment_sum` decimal(15,3) NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `account_id_idx` (`account_id`),
  CONSTRAINT `account_id` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,1,'2015-12-01 21:15:04',100000.000),(2,1,'2016-01-01 18:00:40',100000.000),(3,1,'2016-02-01 11:08:55',100000.000),(4,1,'2016-03-01 14:40:12',100000.000),(5,2,'2015-07-10 08:00:10',50000.000),(6,2,'2015-08-07 21:10:22',50000.000),(7,2,'2015-09-07 16:25:13',50000.000),(8,2,'2015-10-07 12:45:00',50000.000),(9,3,'2016-01-15 17:30:44',220000.000),(10,3,'2016-02-15 18:20:24',220000.000),(11,3,'2016-03-15 20:16:37',220000.000),(12,4,'2015-12-25 11:24:04',160000.000),(13,4,'2016-01-25 00:04:25',160000.000),(14,4,'2016-02-25 17:34:14',160000.000),(15,5,'2015-05-02 21:17:11',70000.000),(16,5,'2015-06-02 18:26:04',70000.000),(17,5,'2015-07-02 15:20:51',70000.000),(18,5,'2015-08-02 11:29:01',70000.000),(19,5,'2015-09-02 14:21:19',70000.000),(20,5,'2015-10-02 21:15:01',70000.000),(21,6,'2015-10-20 21:41:16',130000.000),(22,6,'2015-11-20 08:38:18',130000.000),(23,6,'2015-10-20 20:16:35',130000.000),(24,6,'2015-11-20 12:41:40',130000.000),(25,6,'2015-12-20 10:12:54',130000.000),(26,6,'2016-01-20 16:02:18',130000.000),(27,6,'2016-02-20 00:00:00',130000.000),(28,6,'2016-03-20 18:31:35',130000.000),(29,7,'2016-03-01 16:20:00',220000.000),(30,8,'2015-09-15 22:50:05',70000.000),(31,8,'2015-10-15 07:01:57',70000.000),(32,8,'2015-11-15 15:45:40',70000.000),(33,8,'2015-12-15 20:18:58',70000.000),(34,8,'2016-01-15 00:15:50',70000.000),(35,8,'2016-03-15 16:41:52',70000.000),(36,9,'2016-05-20 00:00:00',150000.000),(37,9,'2016-06-01 00:00:00',70000.000),(38,1,'2016-06-09 18:24:10',5000.000),(39,1,'2016-06-09 18:27:48',5000.000),(40,1,'2016-06-09 18:28:40',5000.000),(41,1,'2016-06-09 18:29:05',5000.000),(42,7,'2016-06-09 19:50:41',20000.000),(43,10,'2016-06-11 15:26:57',100000.000),(44,12,'2016-06-12 15:33:39',50000.000),(45,12,'2016-06-12 15:35:05',100000.000),(46,7,'2016-06-13 14:55:59',50000.000),(47,7,'2016-06-16 14:45:58',77000.000),(48,7,'2016-06-18 15:13:17',100000.000),(49,10,'2016-06-28 13:26:15',50000.000),(50,10,'2016-06-28 13:26:30',50000.000),(51,10,'2016-06-28 14:01:15',30000.000),(52,10,'2016-06-28 14:01:32',30000.000),(53,10,'2016-06-28 14:11:38',20000.000),(54,10,'2016-06-28 14:15:33',25000.000),(55,10,'2016-06-28 14:34:50',5000.000);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tariff_add_requests`
--

DROP TABLE IF EXISTS `tariff_add_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tariff_add_requests` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `tariff_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`request_id`),
  KEY `client_id_idx` (`client_id`),
  KEY `tariff_idx` (`tariff_id`),
  CONSTRAINT `clients_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tariff` FOREIGN KEY (`tariff_id`) REFERENCES `tariffs` (`tariff_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tariff_add_requests`
--

LOCK TABLES `tariff_add_requests` WRITE;
/*!40000 ALTER TABLE `tariff_add_requests` DISABLE KEYS */;
INSERT INTO `tariff_add_requests` VALUES (1,1,3,'address 1'),(9,2,7,'dsljskfk fasdf adf'),(11,17,6,'Беларусь, г Минск, ул. Кальварийская, 45-12'),(12,5,2,'gjfdglj fgjdfglk');
/*!40000 ALTER TABLE `tariff_add_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tariffs`
--

DROP TABLE IF EXISTS `tariffs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tariffs` (
  `tariff_id` int(11) NOT NULL AUTO_INCREMENT,
  `tariff_name` varchar(255) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `currency_code` char(3) NOT NULL,
  `month_payment` decimal(15,3) NOT NULL,
  `upload_speed` float(10,3) NOT NULL,
  `download_speed` float(10,3) NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date DEFAULT NULL,
  `traffic_volume` float(10,3) NOT NULL,
  PRIMARY KEY (`tariff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tariffs`
--

LOCK TABLES `tariffs` WRITE;
/*!40000 ALTER TABLE `tariffs` DISABLE KEYS */;
INSERT INTO `tariffs` VALUES (1,'Эконом','Круглосуточный доступ в интернет со скоростью 10МБит/сек.','BYR',50000.000,10.000,5.000,'2015-06-01','2016-01-01',10.000),(2,'Стандарт','Удобный тарифный план для ежедневного доступа в интернет.','BYR',70000.000,10.000,5.000,'2015-01-20','2016-06-18',20.000),(3,'Безлимит-эконом','Неограниченный доступ в интернет со скоростью 15Мбит/сек.','BYR',100000.000,15.000,10.000,'2015-10-01',NULL,-1.000),(4,'Безлимит-плюс','Неограниченный доступ в интернет со скоростью 25Мбит/сек.','BYR',130000.000,25.000,20.000,'2015-11-05',NULL,-1.000),(5,'Быстрый безлимит','Неограниченный доступ в интернет со скоростью 50Мбит/сек.','BYR',160000.000,50.000,50.000,'2015-12-20',NULL,-1.000),(6,'Супер безлимит','Неограниченный доступ в интернет со скоростью 100Мбит/сек.','BYR',220000.000,100.000,100.000,'2016-01-10',NULL,-1.000),(7,'Звёздный','Невероятно крутой тариф с невероятно высокой скоростью доступа в интернет и минимальной абонентской платой. Спеши подключиться. Предложение ограничено!','BYR',50000.000,150.000,150.000,'2016-06-10',NULL,-1.000),(8,'Новый','Новый тариф','BYR',60000.000,20.000,20.000,'2016-06-16','2016-06-16',10.000),(9,'Супер-быстрый','Супер-быстрый тариф для опфлжовлаожфвлыдаофыва','BYR',20000.000,100.000,100.000,'2016-06-18','2016-06-18',-1.000);
/*!40000 ALTER TABLE `tariffs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traffic_info`
--

DROP TABLE IF EXISTS `traffic_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traffic_info` (
  `account_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date DEFAULT NULL,
  `traffic_used` float(10,3) NOT NULL,
  KEY `account_id_idx` (`account_id`),
  KEY `account_idx` (`account_id`),
  CONSTRAINT `account` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traffic_info`
--

LOCK TABLES `traffic_info` WRITE;
/*!40000 ALTER TABLE `traffic_info` DISABLE KEYS */;
INSERT INTO `traffic_info` VALUES (1,'2015-12-01','2016-01-01',20.000),(1,'2016-01-01','2016-02-01',25.100),(1,'2016-02-01','2016-03-01',22.700),(1,'2016-03-01','2016-04-01',21.000),(1,'2016-04-01',NULL,19.500),(2,'2015-07-10','2015-08-07',9.900),(2,'2015-08-07','2015-09-07',10.000),(2,'2015-09-07','2015-10-07',8.700),(2,'2015-10-07','2015-10-10',1.700),(3,'2016-01-15','2016-02-15',20.500),(3,'2016-02-15','2016-03-15',30.400),(3,'2016-03-15','2016-04-15',29.700),(3,'2016-04-15',NULL,45.500),(4,'2015-12-25','2016-01-25',59.500),(4,'2016-01-25','2016-02-25',79.100),(4,'2016-02-25','2016-03-25',80.300),(4,'2016-03-25',NULL,75.500),(5,'2015-05-02','2015-06-02',14.600),(5,'2015-06-02','2015-07-02',19.400),(5,'2015-07-02','2015-08-02',15.400),(5,'2015-08-02','2015-09-02',18.700),(5,'2015-09-02','2015-10-02',19.300),(5,'2015-10-02','2015-10-15',4.200),(6,'2015-10-20','2015-11-20',51.500),(6,'2015-11-20','2015-12-20',62.100),(6,'2015-10-20','2015-11-20',69.500),(6,'2015-11-20','2015-12-20',81.300),(6,'2015-12-20','2016-01-20',75.200),(6,'2016-01-20','2016-02-20',77.700),(6,'2016-02-20','2016-03-20',63.100),(6,'2016-03-20','2016-04-20',82.400),(6,'2016-04-20',NULL,5.700),(7,'2016-03-01','2016-04-01',95.100),(7,'2016-04-01',NULL,82.000),(8,'2015-09-15','2015-10-15',19.100),(8,'2015-10-15','2015-11-15',18.500),(8,'2015-11-15','2015-12-15',19.400),(8,'2015-12-15','2016-01-15',17.300),(8,'2016-01-15','2016-02-15',18.100),(8,'2016-03-15','2016-04-15',20.000),(8,'2016-04-15',NULL,19.600);
/*!40000 ALTER TABLE `traffic_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-28 18:52:28
